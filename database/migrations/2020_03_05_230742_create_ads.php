<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->string('description',2000);
            $table->integer('price');
            $table->string('currency');
            $table->string('phone');
            $table->integer('category_id')->nullable(); //foreign key in Categories
            $table->integer('city_id')->nullable(); //foreign key in Cities
            $table->integer('status')->default(0); //0 en revision, 1 aprobado, 2 rechazado
            $table->dateTime('sold')->nullable(); //sold date
            $table->integer('views')->default(0); //number of times that an ad has been visited
            $table->dateTime('boost')->nullable(); //date and time of a boost to put this result at the top of the "Anuncios promocionados"
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}

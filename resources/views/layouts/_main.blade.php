<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KFH8XB9');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>{{ !empty($title_page) ? $title_page : 'hoylovendo.com' }}</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body{
            background-color: #FFF;
        }
        .category-icon{
            background-color:#f9f9f9;height:100px; width:100px;margin:5px auto;display:block;
        }
        .category-icon-img{
            width:36px; margin:auto; display:block;padding-top:15px;
        }
        .border-blue{
            border-bottom:1px solid #3490dc;
        }
        .border-grey{
            border-bottom:1px solid #ccc;
        }

        .top-1rem{
            margin-top:1rem;
        }
        .top-06rem{
            margin-top:0.6rem;
        }
        .fontsmall{
            font-size:0.8rem;
            color:#ccc;
        }
        .hlv-card{
            -webkit-border-radius: 10px; 
            border-radius: 10px; 
            -webkit-box-shadow: 0 0px 2px 0 rgba(0, 0, 0, 0.14), 0 0px 30px 0 rgba(0, 0, 0, 0.14);
            box-shadow: 0 0px 2px 0 rgba(0, 0, 0, 0.14), 0 0px 30px 0 rgba(0, 0, 0, 0.14);
            cursor:pointer;
            text-decoration:none;
        }
        .hlv-card:hover{
            -webkit-border-radius: 10px; 
            border-radius: 10px; 
            -webkit-box-shadow: 0 0px 4px 0 rgba(0, 0, 0, 0.24), 0 0px 30px 0 rgba(0, 0, 0, 0.24);
            box-shadow: 0 0px 4px 0 rgba(0, 0, 0, 0.24), 0 0px 30px 0 rgba(0, 0, 0, 0.24);
            text-decoration:none;
        } 
        .selected-category{
            background-color: #fff;
            border: 1px solid #ccc;
        }

        @media (min-width: 300px){
            .carousel-max-height{
                background-color:#ccc; 
                max-height:250px;
            }
            .card-responsive-padding{
                padding: 6px 3px 0px 3px;
            }
            .label-cat{
                display:none;
                padding-top:9px; 
                font-size:9px;
            }
            .category-icon{
                height: 60px;
                width: 60px;
                margin: 5px auto;
                display: block;
            }
            .hide-small{
                display:none;
            }
        }
        @media (min-width: 576px){
            .carousel-max-height{
                background-color:#ccc; 
                max-height:300px;
            }
            .card-responsive-padding{
                padding: 8px 4px 0px 4px;
            }
            .label-cat{
                display:none;
                padding-top:9px; 
                font-size:9px;
            }
            .category-icon{
                height: 80px;
                width: 80px;
                margin: 8px auto;
                display: block;
            }
            .hide-small{
                display:none;
            }
        }
        @media (min-width: 992px){
            .sold-big{
                width:100px;
            }
            .carousel-max-height{
                background-color:#ccc; 
                max-height:400px;
            }
            .card-responsive-padding{
                padding: 20px 10px 0px 10px;
            }
            .label-cat{
                display:inLine;
                padding-top:9px; 
                font-size:11px;
            }
            .category-icon{
                height: 100px;
                width: 100px;
                margin: 10px auto;
                display: block;
            }
            .hide-small{
                display:inLine;
            }
        }

        @media (min-width: 1200px){
            .sold-big{
                width:120px;
            }
            .carousel-max-height{
                background-color:#ccc; 
                max-height:600px;
            }
            .card-responsive-padding{
                padding: 30px 15px 0px 15px;
            }
            .label-cat{
                display:inLine;
                padding-top:10px; 
                font-size:12px;
            }
            .category-icon{
                height: 100px;
                width: 100px;
                margin: 10px auto;
                display: block;
            }
            .hide-small{
                display:inLine;
            }
        }  
        
        .filter-select{
            width: auto;
            height: 2.2rem;
            background-color: #3490dc;
            color: #fff;
            border: 0px;
            margin: 0px 10px;
        }

        .swal-footer{
            text-align: center;
        }

        .swal-button--fbook{
            color: #FFF;
            background-color: #4963A4;
        }

        .swal-button--gmail{
            color: #FFF;
            background-color: #4385F4;
        }
        .swal-button:focus{
            box-shadow:none;
        }
        .growtext{
            font-size:1.2rem;
        }
    </style>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" ></script>
    <script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js" defer></script>
    <!-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> -->
    @yield('jstools')

</head>
<body id="override-bootstrap">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFH8XB9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @yield('content')
    @include('partials.footer')
</body>

</html>

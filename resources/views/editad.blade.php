@extends('layouts._main')

@section('jstools')
@endsection

@include('partials.navbar1')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="border-blue">Editar anuncio</h2>
                </div>
            </div>
            <div class="row justify-content-center top-1rem">
                <div class="col-md-12">
                    <form action={{url('/publishpost')}} method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="título">Titulo:</label> 
                            <input id="título" name="título" type="text" class="form-control @error('title') is-invalid @enderror" value="{{old('título')}}" placeholder="Qué es lo que quieres vender?">
                            @error('título')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="descripción">Descripción:</label> 
                            <textarea id="descripción" name="descripción" class="form-control @error('description') is-invalid @enderror" rows="4" placeholder="Detalla las características del producto o servicio que ofreces.">{{old('descripción')}}</textarea>
                            @error('descripción')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="precio" style="display:block;">Precio</label> 
                            <input id="precio" name="precio" type="text" class="form-control col-4 @error('price') is-invalid @enderror" style="display:inline;" value="{{old('precio')}}">
                            <select id="moneda" name="moneda">
                                <option value="$us." {{old('moneda')=='$us.'?'selected':''}} >Dólares ($us.)</option>
                                <option value="Bs." {{old('moneda')=='Bs.'?'selected':''}}  >Bolivianos (Bs.)</option>
                            </select>
                            @error('precio')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="teléfono" style="display:block;">Teléfono: </label> 
                            <input id="teléfono" name="teléfono" type="teléfono" class="form-control  @error('phone') is-invalid @enderror" style="display:inline;" value="{{old('teléfono')}}" placeholder="A qué número quieres que te llamen">
                            @error('teléfono')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group" style="padding-top:0.9rem;">
                            <button type="submit" id="formsubmit" class="btn btn-primary">Crear anuncio</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        
    </div>
</div>
@endsection
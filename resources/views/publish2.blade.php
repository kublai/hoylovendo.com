@extends('layouts._main')

@section('jstools')

@endsection

@section('content')
<div id="app">
    <div id="loader" style="display:none; position:absolute; top:0px; left:0px; background-color:#fff; width:100%; height:100%; z-index:100; opacity:1;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 text-center">
                    <h4 style="margin-top:1rem;">Procesando imagenes...</h4>
                    <img class="img-fluid" src="/img/uploading-photos.png">
                    <p>Subiendo imagenes al servidor</p>
                </div>
                <div class="col-12 col-md-8" style="height: 20px; margin-top:1rem;">
                    <div class="progress" style="height:20px">
                        <div id="progress1" class="progress-bar" role="progressbar" style="width: 0%; height:20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>   
                    </div>                   
                </div>
                <div class="col-12 col-md-8" style="height: 20px; margin-top:1rem;">
                    <div class="progress" style="height:20px">
                        <div id="progress2" class="progress-bar" role="progressbar" style="width: 0%; height:20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>                        
                    </div>
                </div>
                <div class="col-12 col-md-8 " style="height: 20px; margin-top:1rem;">
                    <div class="progress" style="height:20px">
                        <div id="progress3" class="progress-bar" role="progressbar" style="width: 0%; height:20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div> 
                    </div>  
                </div>
                <div class="col-12 col-md-8 " style="height: 20px; margin-top:1rem;">
                    <div class="progress" style="height:20px">
                        <div id="progress4" class="progress-bar" role="progressbar" style="width: 0%; height:20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>  
                    </div>
                </div>
                <div v-if="inprocessComputed" class="col-12 col-md-8 text-center " style="margin-top:1rem;">
                    <p>Hecho, ¡Ya tienes tu anuncio!</p>
                    <button class="btn btn-success" @click="finishOk">Publicar anuncio</button>
                    
                </div>
                <div v-else class="col-12 col-md-8 text-center " style="margin-top:1rem;">
                    <p>Ten paciencia, este proceso depende del tamaño de las fotografías y la velocidad de tu conexión.</p>
                    <button class="btn btn-danger" @click="cancelUpload">Cancelar proceso</button>
                </div>
                
            </div>
        </div>
    </div>

    <div class="container" >
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                    @if ($mode =="create")
                        <div class="row" style="margin-top:1rem;">
                            <div class="col-6 border-blue" style="background-color:#fff; color:#ddd;">
                                <span style="font-size:2rem">1</span> Crear el texto
                            </div>
                            <div class="col-6 border-blue" style="background-color:#3490dc; color:#fff;">
                                <span style="font-size:2rem">2</span> Añadir fotos
                            </div>
                        </div>
                        <img src="/img/camera.png"  style="width:60px; margin:auto; display:block; margin-top:0.5rem;"/>
                        <p class="text-center">Puedes usar tu cámara de tu celular para hacer las fotos.</p>
                    @else
                        <h2 class="border-blue">Editar anuncio</h2>
                        <p>Si quieres, puedes añadir o eliminar fotos antes de terminar.</p>
                    @endif
                    </div>
                </div>
                @if ($mode =="edit")
                    @foreach($photos as $photo)
                        <div style="display:inline; padding:10px;"><img src="/public/200/{{$photo->file}}"></div>
                    @endforeach
                @endif
                <div class="row justify-content-center top-1rem">
                    <form action="#" method="POST" enctype="multipart/form-data" @submit="submitImages">
                        @csrf
                        <input type="hidden" id="ad_id" name="ad_id" value="{{$ad_id}}">
                        <div class="form-group" style="padding-top:0.6rem;">
                            <input type="file" class="form-control-file" id="foto1" name="foto1"  style="margin-bottom:15px; padding:4px;" value="{{old('foto1')}}">
                            @error('foto1')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input type="file" class="form-control-file" id="foto2" name="foto2" style="margin-bottom:15px; padding:4px;">
                            @error('foto2')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input type="file" class="form-control-file" id="foto3" name="foto3" style="margin-bottom:15px; padding:4px;">
                            @error('foto3')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input type="file" class="form-control-file" id="foto4" name="foto4" style="margin-bottom:15px; padding:4px;">
                            @error('foto3')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group text-center" style="padding-top:0.9rem;" class=>
                            <button type="submit" id="formsubmit" class="btn btn-primary">Publicar anuncio</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
    data:{
        imgCounter: 0,
        imgCounterCtrl:0,
    },
    computed:{
        inprocessComputed: function(){
            if(this.imgCounter == this.imgCounterCtrl){
                return true;
            }else{
                return false;
            }
        },
    },
    methods:{
        cancelUpload: function(){
            location.reload();
        },
        finishOk: function(){
            window.location.href ="/dashboard";
        },
        submitImages: function(event){
            event.preventDefault();
            f1 = document.getElementById('foto1');
            f2 = document.getElementById('foto2');
            f3 = document.getElementById('foto3');
            f4 = document.getElementById('foto4');
            if(f1.files.length + f2.files.length + f3.files.length + f4.files.length == 0){
                swal("Debes subir al menos una foto");
                return;
            }
            window.scrollTo(0,0);
            document.getElementById('loader').style.display="inline";
            var progress1 = document.getElementById("progress1");
            var progress2 = document.getElementById("progress2");
            var progress3 = document.getElementById("progress3");
            var progress4 = document.getElementById("progress4");
            var _this = this;

            var config1 = {
                onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                progress1.setAttribute('aria-valuenow', percentCompleted);
                progress1.style.width=percentCompleted+"%";
                }
            };
            var config2 = {
                onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                progress2.setAttribute('aria-valuenow', percentCompleted);
                progress2.style.width=percentCompleted+"%";
                }
            };
            var config3 = {
                onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                progress3.setAttribute('aria-valuenow', percentCompleted);
                progress3.style.width=percentCompleted+"%";
                }
            };           
            var config4 = {
                onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                progress4.setAttribute('aria-valuenow', percentCompleted);
                progress4.style.width=percentCompleted+"%";
                }
            };  

            //all photos are uploaded as main, only the last one to finish will keep this status          
            if (f1.files.length>0){
                this.imgCounter++;
                var data1 = new FormData();
                data1.append('ad_id',{{$ad_id}});
                data1.append('foto', f1.files[0]);
                data1.append('main', 1);
                data1.append('counter', 1);
                axios.post("{{url('/publishpostphotos')}}", data1, config1)
                    .then(function (res) {
                        progress1.innerHTML = "Imagen cargada correctamente";
                        _this.imgCounterCtrl++;
                    })
                    .catch(function (err) {
                        progress1.className="progress-bar bg-danger";
                        progress1.innerHTML = "Ups.., ERROR al subir esta imágen";
                        _this.imgCounterCtrl++;
                    });
            }
            if (f2.files.length> 0){
                this.imgCounter++;
                var data2 = new FormData();
                data2.append('ad_id',{{$ad_id}});
                data2.append('foto', f2.files[0]);
                data2.append('main', 1);
                data2.append('counter', 2);
                axios.post("{{url('/publishpostphotos')}}", data2, config2)
                    .then(function (res) {
                        progress2.innerHTML = "Imagen cargada correctamente";
                        _this.imgCounterCtrl++;
                    })
                    .catch(function (err) {
                        progress2.className="progress-bar bg-danger";
                        progress2.innerHTML = "Ups.., ERROR al subir esta imágen";
                        _this.imgCounterCtrl++;
                    });
            }
            if (f3.files.length> 0){
                this.imgCounter++;
                var data3 = new FormData();
                data3.append('ad_id',{{$ad_id}});
                data3.append('foto', f3.files[0]);
                data3.append('main', 1);
                data3.append('counter', 3);
                axios.post("{{url('/publishpostphotos')}}", data3, config3)
                    .then(function (res) {
                        progress3.innerHTML = "Imagen cargada correctamente";
                        _this.imgCounterCtrl++;
                    })
                    .catch(function (err) {
                        progress3.className="progress-bar bg-danger";
                        progress3.innerHTML = "Ups.., ERROR al subir esta imágen";
                        _this.imgCounterCtrl++;
                    });
            }
            if (f4.files.length> 0){
                this.imgCounter++;
                var data4 = new FormData();
                data4.append('ad_id',{{$ad_id}});
                data4.append('foto', f4.files[0]);
                data4.append('main', 1);
                data4.append('counter', 4);
                axios.post("{{url('/publishpostphotos')}}", data4, config4)
                    .then(function (res) {
                        progress4.innerHTML = "Imagen cargada correctamente";
                        _this.imgCounterCtrl++;
                    })
                    .catch(function (err) {
                        progress4.className="progress-bar bg-danger";
                        progress4.innerHTML = "Ups.., ERROR al subir esta imágen";
                        _this.imgCounterCtrl++;
                    });
            }

        }
    }
});
</script>
@endsection

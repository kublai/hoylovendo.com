@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')
<div class="container" id="app">

    <div class="col-md-12 top-1rem">
        <h2 class="border-blue">Términos y condiciones de uso</h2>
    </div>

    <div class=col-12>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >TÉRMINOS Y CONDICIONES DE USO DE LAS PÁGINAS DEL SITIO WEB O LA
        APLICACIÓN MÓVIL (APP) HOYLOVENDO.COM<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >El uso de nuestros servicios a través del sitio Web hoylovendo.com
        manifiesta la aceptación de estos términos y condiciones de uso. Por favor leer
        este documento cuidadosamente, en el se describen los términos y condiciones
        generales aplicables al uso de los diferentes servicios brindados por
        hoylovendo.com <span style='color:#C9211E'>(ej. publicidad para productos o
        servicios a través de anuncios, membresía, tiendas virtuales, banners publicitarios)</span>
        servicios brindados tanto en el sitio web. hoylovendo.com o su aplicación
        movil(app), adicionalmente a los servicios de publicidad anteriormente
        mencionados, hoylovendo.com podrá ofrecer otro tipo de servicios, los cuales
        están igualmente sujetos a los términos y condiciones generales de uso y las
        condiciones generales de uso y las condiciones particulares aplicables al
        servicio.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Cualquier persona natural o jurídica que desee acceder y/o usar los
        servicios que ofrece hoylovendo.com, y cuente con capacidad legal para
        contratar, podrá hacerlo sujetándose a los términos y condiciones de uso de
        hoylovendo.com, las políticas de privacidad y cada una de las clausulas que se
        presentan y que en adelante se detallan.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >No podrán utilizar hoylovendo.com los menores de edad y/o usuarios
        que hayan sido suspendidos temporalmente o inhabilitados definitivamente por
        hoylovendo.com, en caso de que el usuario este registrado en representación de
        Persona Jurídica, usted debe tener la capacidad legal para efectuar contratos a
        nombre de la persona jurídica y de obligar a la misma en los términos de este
        contrato.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >CUALQUIER PERSONA NATURAL O JURÍDICA QUE NO ACEPTE LO ANTERIORMENTE
        MENCIONADO Y LO QUE SE DETALLA A CONTINUACIÓN, DEBERÁ ABSTENERSE DE UTILIZAR
        LOS SERVICIOS OFRECIDOS POR HOYLOVENDO.COM (SITIO WEB, APLICACIÓN MÓVIL O
        CUALQUIER OTRO TIPO DE SERVICIO)</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        ><o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >1. REGISTRO DEL USUARIO<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Para acceder a todos los servicios ofrecidos por hoylovendo.com,
        será obligatorio completar los datos solicitados en todos sus campos, siendo de
        absoluta responsabilidad del usuario que los datos brindados sean válidos,
        exactos, precisos, verdaderos y auténticos, comprometiéndose a su debida<span
        style='mso-spacerun:yes'>   </span>actualización conforme resulte necesario.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Hoylovendo.com podrá utilizar diversos medios para identificar al
        usuario, hoylovendo.com no se responsabiliza por la certeza de los datos
        personales provistos por el usuario al momento de su registros o uso de los
        servicios ofrecidos, como se estipula en el párrafo anterior.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Hoylovendo.com, se reserva el derecho de solicitar al usuario
        comprobantes y/o datos adicionales a efecto de corroborar su registro y
        publicación, así como de suspender temporal o definitivamente aquel usuario
        cuyos datos no hayan podido ser confirmados.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >De igual manera se estipula que, en estos casos de deshabilitación
        de un usuario, se darán de baja todos los artículos publicados por el usuario,
        así como todas sus ofertas e interacciones sin que ello genere derecho a
        resarcimiento económico alguno por parte de hoylovendo.com</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >El usuario accederá a una cuenta personal desde la cual podrá
        utilizar los servicios ofrecidos por hoylovendo.com, está será única e
        intransferible, siendo el usuario responsable de todas las operaciones
        efectuadas en su cuenta, dado que el acceso a la misma está restringido al
        ingreso y uso de la clave de seguridad que es de conocimiento exclusivo del
        usuario. El usuario se compromete a notificar a hoylovendo.com de manera
        inmediata y por médico idóneo y fehaciente, cualquier uso no autorizado de su
        cuenta.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >En la medida en que así lo permita la legislación aplicable, el
        usuario acepta asumir la responsabilidad que proceda por todas las actividades
        realizadas desde su cuenta o utilizando su contraseña, hoylovendo.com se
        reserva el derecho de rechazar cualquier solicitud de servicio o de la
        cancelación de un servicio previamente aceptada, comunicando las razones de su
        decisión sin que ello genere algún derecho a indemnización o resarcimiento.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >PRIVACIDAD DE LA INFORMACIÓN<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Toda la información que el usuario proporciona el momento de
        registrarse se procesa y almacena en servidores o medios magnéticos que mantienen
        altos estándares de seguridad y protección tanto física como tecnológica.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Sin la autorización escrita de hoylovendo.com, está completamente
        prohibida la recopilación de anuncios, información de usuarios, copiar ni pegar
        nuestro contenido, realizar copias parciales o completas citando o no la
        fuente, violar el uso de marca y logo de nuestros usuarios o clientes, etc, y
        el envío por cualquier medio digital o no a terceras personas o a otros sitios
        Web.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Al proporcionarnos su información personal, consiente que recabemos
        y procesemos sus datos<span style='mso-spacerun:yes'>  </span>personales de la
        siguiente manera:</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l7 level1 lfo1'><![if !supportLists]><span
        style='font-family:OpenSymbol;mso-fareast-font-family:OpenSymbol;
        mso-bidi-font-family:OpenSymbol'><span style='mso-list:Ignore'>•<span
        style='font:7.0pt "Times New Roman"'> </span></span></span><![endif]><span
        >Otorga la facultad de recopilar y almacenar su información personal
        como por ejemplo su nombre, dirección de correo electrónico, teléfono, genero,
        rango de edad, discusiones, conversaciones, disputas y correspondencia que
        realice a través de hoylovendo.com, así como la correspondencia que nos envíe;
        datos sobre su ordenador y sus conexiones, las estadísticas de sus consultas de
        páginas, como su dirección IP <span style='color:#C9211E'>información de registro
        Web estándar</span> y cualquier otra información suplementaria que nos
        proporcione.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l7 level1 lfo1'><![if !supportLists]><span
        style='font-family:OpenSymbol;mso-fareast-font-family:OpenSymbol;
        mso-bidi-font-family:OpenSymbol'><span style='mso-list:Ignore'>•<span
        style='font:7.0pt "Times New Roman"'> </span></span></span><![endif]><span
        >Así mismo hoylovendo.com podrá contactarlo a través de las dirección
        de correo electrónico que nos proveyó para mantenerle informado acerca de
        nuestras promociones, nuevos servicios, nuevas políticas o cualquier otra
        información que consideremos relevante.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l7 level1 lfo1'><![if !supportLists]><span
        style='font-family:OpenSymbol;mso-fareast-font-family:OpenSymbol;
        mso-bidi-font-family:OpenSymbol'><span style='mso-list:Ignore'>•<span
        style='font:7.0pt "Times New Roman"'> </span></span></span><![endif]><span
        >Otorga la facultad de revelar su información personal para responder
        a requerimientos legales, obligar el cumplimiento de nuestras políticas,
        responder a reclamaciones que afirmen que un anuncio u otro contenido infringe
        los derechos de terceros o proteger los derechos, la propiedad o la seguridad
        de otras personas.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l7 level1 lfo1'><![if !supportLists]><span
        style='font-family:OpenSymbol;mso-fareast-font-family:OpenSymbol;
        mso-bidi-font-family:OpenSymbol'><span style='mso-list:Ignore'>•<span
        style='font:7.0pt "Times New Roman"'> </span></span></span><![endif]><span
        >No revelaremos su información personal a representantes
        gubernamentales, agentes de la ley o terceras partes<span
        style='mso-spacerun:yes'>  </span>sin una citación, una orden judicial o un
        procedimiento similar, excepto cuando se crea de buena fe que la revelación de
        información sea necesaria para impedir un daño físico, una pérdida financiera
        inminentes o para informar acerca de una supuesta actividad ilegal.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >DERECHO DE ELIMINACIÓN DE SU PERFIL Y SUS ANUNCIOS<o:p></o:p></span></b></p>
        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l7 level1 lfo1'><![if !supportLists]><span
        style='font-family:OpenSymbol;mso-fareast-font-family:OpenSymbol;
        mso-bidi-font-family:OpenSymbol'><span style='mso-list:Ignore'>•<span
        style='font:7.0pt "Times New Roman"'> </span></span></span><![endif]><span>
            Si desea eliminar su perfil así como todos los anuncios que haya publicado en nuestra plataforma, escriba un correo electrónico a info@bytebox.es
            indicando su nombre completo, un número de teléfono y el tipo de acceso que ha usado para darse de alta (Facebook / Google ), nos pondremos
            en contacto con usted para confirmar la solicitud y eliminaremos su perfil y todo el contenido asociado de nuestra base de datos.<br>
            Recuerde que los datos serán eliminado fisicamente y no podremos reponerlos una vez que hayan sido eliminados.</span></p>
        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >VIOLACIONES DEL SISTEMA O BASE DE DATOS<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >No está permitida ninguna acción o uso de dispositivo, sofware, u
        otro medio tendiente a interferir tanto en las actividades y <span
        style='color:#C9211E'>operatividad</span> de hoylovendo.com como en las
        ofertas, descripciones, cuentas o bases de datos de hoylovendo.com. Cualquier
        intromisión, tentativa o actividad violatoria o contraria a las leyes sobre
        derechos de propiedad intelectual y/o a las prohibiciones estipuladas en este
        contrato harán pasible a su responsable de las acciones legales pertinentes, y
        a las sanciones previstas por este acuerdo, así como lo hará responsable de
        indemnizar los daños ocasionados.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >FALLAS EN EL SISTEMA<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        style='color:#C9211E'>hoylovendo.com no se responsabiliza por
        cualquier daño, perjuicio o pérdida al usuario causados por fallas en el
        sistema, en el servidor o en internet.<span style='mso-spacerun:yes'> 
        </span>Hoylovendo.com tampoco será responsable por cualquier virus que pudiera
        infectar el equipo del usuario como consecuencia del acceso, uso o examen del
        sitio web o a raíz de cualquier transferencia de datos, archivos, imágenes,
        textos o audio contenidos en el mismo.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Los usuarios NO podrán imputarle responsabilidad alguna ni exigir
        pago por lucro cesante, en virtud de perjuicios resultantes de dificultades
        técnicas o fallas en los sistemas o en internet.<span
        style='mso-spacerun:yes'>  </span>Hoylovendo.com no garantiza el acceso y uso
        continuado o ininterrumpido de su sitio.<span style='mso-spacerun:yes'> 
        </span>El sistema puede eventualmente no estar disponible debido a dificultades
        técnicas o fallas de internet, o por cualquier otra circunstancia ajena a hoylovendo.com;
        en tales casos se procurará restablecerlo con la mayor celeridad posible sin
        que por ello pueda imputársele algún tipo de responsabilidad.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Hoylovendo.com NO será responsable por ningún error u omisión
        contenidos en su sitio.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >OBLIGACIONES DE LOS USUARIOS<o:p></o:p></span></b></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l6 level1 lfo2'><b>1. Sobre la Publicación de
        bienes y/o servicios.-</b> El usuario deberá ofrecer
        la venta de los bienes y promover los servicios en las categorías apropiadas,
        ofertando un solo producto por anuncio creado. Las publicaciones deberán incluir texto descriptivo, fotografías, precio
        y datos de contacto, siempre que no violen ninguna disposición de este acuerdo
        o demás políticas de hoylovendo.com.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'>
        El producto ofrecido por el usuario ofertante debe ser exactamente
        descrito en cuanto a sus condiciones y características relevantes. Se entiende y presume que mediante la
        inclusión del bien o servicio en hoylovendo.com, el usuario ofertante acepta
        que tiene la intensión y el derecho de vender el bien por él ofrecido, o está
        facultado para ello por el titular y lo tiene disponible para su entrega
        inmediata. No obstante, a lo mencionado se establece que los precios de los
        productos publicados deberán ser expresados con IVA incluido cuando corresponda
        la aplicación del mismo, y en moneda de curso legal.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'>
        Hoylovendo.com, podrá remover cualquier publicación cuyo precio no
        sea expresado de esta forma para evitar confusiones o malos entendidos en
        cuanto al precio final del producto. De
        igual manera se establece que el medio de pago será entre las partes que
        participan de la compra venta, hoylovendo.com no gestiona la transacción entre
        partes.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'>
        En caso que se infrinja cualquiera de las disposiciones establecidas
        en esta cláusula, hoylovendo.com podrá editar el espacio, solicitar al usuario
        ofertante que lo edite, o dar de baja la publicación donde se encuentre la
        infracción.</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l6 level1 lfo2'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><b><span>Sobre las Obligaciones del
        Usuario Comprador. - </span></b><span>El usuario comprador debe
        tener capacidad legal para comprar el bien y/o servicios ofrecidos por los
        usuarios ofertantes. Asimismo, deberá únicamente ofertar por artículos
        permitidos. De igual manera se compromete a realizar ofertas veraces para la
        compra de los bienes o servicios ofrecidos por los usuarios ofertantes.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Los artículos o servicios prohibidos, los encontrarán detallados en
        el acápite “Artículos<b> Prohibidos”</b> de hoylovendo.com.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Durante el plazo fijado por el usuario ofertante, el usuario
        comprador podrá realizar una o varias ofertas de compra para los bienes y/o
        servicios ofertados por el anunciante. El anuncio se publicará hasta 6 meses en
        la plataforma al cabo de este tiempo será dado de baja automáticamente<span
        style='color:#C9211E'>.</span> Se deja claramente establecido que
        hoylovendo.com es un punto de encuentro entre el usuario comprador y el usuario
        ofertante, y dado que no participa de las operaciones que se realizan entre
        ellos no es responsable por ninguna de las actividades posteriores a su
        contacto.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Tampoco se ofrecen garantías a ninguna de las partes ante la
        imposibilidad de supervisar la veracidad de los datos. Es responsabilidad de los
        usuarios (comprador/vendedor, ofertante/demandante), revisar la veracidad de la
        información de acuerdo a su conveniencia. En caso de incumplimiento del mismo,
        el usuario afectado podrá iniciar las acciones correspondientes ante las
        instancias que corresponden, excluyendo completamente a hoylovendo.com de toda
        responsabilidad.<b><o:p></o:p></b></span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l6 level1 lfo2'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><b><span>Sobre las Obligaciones del
        Usuario Ofertante.-</span></b><span><span style='mso-spacerun:yes'> 
        </span>El usuario ofertante debe tener capacidad legal para vender el bien y/o
        servicio ofertado.<span style='mso-spacerun:yes'>  </span>Asimismo deberá
        cumplir con publicar únicamente los artículos permitidos y proporcionar
        información real en las publicaciones de sus anuncios por el tiempo permitido
        por hoylovendo.com.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Como se menciona anteriormente, hoylovendo.com sólo pone a
        disposición de los usuarios un espacio virtual que les permite comunicarse
        mediante internet para encontrar una forma de vender o comprar bienes y/o
        servicios. Hoylovendo.com no tiene participación alguna en el proceso de
        negociación y perfeccionamiento del contrato ya sean escritos o acuerdos
        verbales entre las partes. Por ende, hoylovendo.com no es responsable por el
        efectivo cumplimiento de las obligaciones fiscales o impositivas establecidas
        por la ley vigente o de cualquier tipo de controversia que origine entre ellas.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        style='color:#C9211E'>En cuanto a contratos de servicios,
        hoylovendo.com no interviene en los procesos de selección efectuados por medio
        de nuestra pagina. Queda aclarado, que la selección ni el reclutamiento de
        personal no nos corresponde.<o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        style='color:#C9211E'><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >PROHIBICIONES<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >Los usuarios no podrán:<o:p></o:p></span></b></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>1. Manipular los precios de los
        bienes y/o servicios ofertados;</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>2. Dar a conocer sus datos
        personales o de otros usuarios por ningún medio.</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>3. Publicar o vender artículos
        prohibidos que se especifican en este documento o leyes vigentes.</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>4. Insultar o agredir a otros
        usuarios;</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>5. Ofertar mas de un bien y/o servicio por
        anuncio, esto incluye titulo y contenido.</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>6. Al usar hoylovendo.com, se
        prohíbe la oferta de artículos o anuncios que hagan publicidad y/o promuevan
        temas relacionados con Política, Sexo, Religión, Violencia, Sustancias
        Controladas (Ley 1008), o incumplan con la ley contra el Racismo y todo Acto de
        Discriminación (Ley N.º 045).</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l3 level1 lfo3'>7. Este tipo de actividades será
        investigado por hoylovendo.com y sus derechos adquiridos por su membresía y/o
        de cualquier otra forma que estime pertinente, sin perjuicio de las acciones
        legales a que pueda dar lugar por la configuración de delitos o contravenciones
        o los perjuicios civiles y/o penales que pueda causar a los usuarios.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >LISTADO DE BIENES<o:p></o:p></span></b></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l2 level1 lfo4'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><b><span>Publicación de bienes y/o
        servicios .- </span></b><span>El usuario podrá ofrecer la venta de
        bienes y servicios de acuerdo a la categoría apropiada <span style='color:#C9211E'>ofertando
        un solo anuncio por producto creado.</span> Las publicaciones podrán incluir
        texto descriptivo, fotografiás y otros datos pertinentes para la venta del bien
        o promoción del servicio, siempre que no violen ninguna disposición de este
        acuerdo.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >El producto ofrecido por el usuario ofertante, debe ser exactamente
        descrito en cuanto a sus condiciones y características relevantes. Se entiende
        y presume que mediante la inclusión del bien o servicio en hoylovendo.com, el
        usuario ofertante acepta que tiene la intención y el derecho de vender el bien
        por él ofrecido, o esta facultado para ello por su titular y lo tiene
        disponible para su entrega inmediata. No obstante, a lo mencionado se establece
        que los precios de los productos publicados deberán ser expresados con IVA
        incluido cuando corresponda la aplicación del mismo, y en moneda del curso
        legal, para evitar confusiones y malos entendidos en cuanto al precio final del
        producto, hoylovendo.com podrá remover cualquier publicación que no sea
        expresada de esta forma.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l2 level1 lfo4'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><b><span>Inclusión de imágenes y
        fotografías .- </span></b><span>El usuario ofertante puede incluir
        fotografías e imágenes correspondientes al producto o servicio ofrecido, una
        fotografía como mínimo es obligatoria. Queda completamente prohibida la
        publicidad con menores de edad y personas que no hayan autorizado el uso de su
        imagen. Hoylovendo.com podrá impedir la publicación de las mencionadas
        fotografiás y la publicación a la cual correspondan, si interpretara , a su
        exclusivo criterio, que la imagen no cumple con los presentes términos y
        condiciones de uso.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l2 level1 lfo4'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><b><span>Artículos Prohibidos .-</span></b><span
        > Sólo podrán ser ingresados en las listas de bienes ofrecidos y/o
        servicios promocionados, aquellos cuya venta no se encuentre tácita o
        expresamente prohibida por ley vigente, normas que atenten contra la salud, la
        seguridad de las personas y del Estado, la moral y las buenas costumbres. Aquellos
        anuncios que hoylovendo.com considere de forma unilateral que no cumplen con
        los criterios establecidos serán eliminados de la plataforma.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >En hoylovendo.com, usted NO deberá colocar publicidad, requerimiento
        o venta de artículos, servicios u otros prohibidos que atenten contra las
        normas vigentes o políticas de hoylovendo.com expresadas en esté documento.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>a)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Cualquier tipo de arma de
        fuego, municiones, replicas, armas blancas o punzo cortantes, objetos de
        interés militar, material explosivo, armas de balines, armas de aire
        comprimido, silenciadores o cualquier otro objeto que se anuncie como apto para
        ser usado como arma o incitando aun comportamiento violento con su utilización.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>b)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Estupefacientes, drogas,
        alucinógenos o cualquier tipo de sustancia no permitida por las leyes en
        vigencia ni los utensilios para su uso modificación, proceso o consumo. Se
        incluye en esta restricción cualquier articulo que promueva o incite a su
        producción, consumo<span style='mso-spacerun:yes'>  </span>o distribución.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>c)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Artículos relacionados con la
        policía.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>d)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Medicamentos y artículos
        farmacéuticos sujetos a prescripción médica.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>e)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Anabólicos y esteroides.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>f)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Está absolutamente prohibida la
        venta de propiedades o artículos robados, ya que esa operación infringe la ley
        en todo el territorio de nuestro país.Hoylovendo.com está comprometido en la
        acción de la justicia destinada a recuperar o investigar la procedencia,
        propiedad y destino de la propiedad robada, por lo que si en algún momento
        alguna autoridad exige información sobre algún artículo o publicación
        procederemos a brindarles los datos de la misma y el usuario
        correspondiente.<span style='mso-spacerun:yes'>  </span>Dentro de propiedad
        robada se incluyen artículos de individuos privados, así como las propiedades
        de empresas y gobiernos adquiridas sin autorización del o los propietarios.
        Ante la sospecha de que un artículo ofrecido en hoylovendo.com es robado, contactaremos
        inmediatamente con las autoridades competentes para que investiguen la
        situación.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>g)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Está prohibida la oferta de
        personas, cuerpos humanos, cadáveres, órganos, miembros o residuos humanos.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>h)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>NO se podrá ofrecer animales,
        piales de animales, fauna salvaje o especies en vías de extinción controladas
        por las leyes vigentes.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>i)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Monedas, billetes o estampillas
        falsificadas</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>j)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Artículos falsificados o
        adulterados.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>k)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Artículos de contrabando</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>l)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Acciones, bonos o valores.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>m)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Bases de datos personales y/o
        Listas de correos.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>n)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Servicios y/o artículos
        relacionados con la pornografía, prostitución o trata de blancas.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>o)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Promover artículos que inciten
        a la violencia.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>p)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Promover artículos que inciten
        a ala discriminación.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>q)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Artículos que violen los
        derechos de autor.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>r)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Decodificadores de televisión.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>s)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Servicios de desbloqueo de
        celulares y equipos de comunicación.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>t)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Documentos de identidad, como
        ser células de identidad, pasaportes, licencias de conducir, certificados de
        nacimiento, tarjetas de crédito o débito o cualquier otro documento similar a
        los mencionados.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>u)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Patrimonio Histórico.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>v)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Hacking y cracking.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>w)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Reventa de entradas para
        espectáculos públicos y privados.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>x)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Se removerá cualquier artículo
        que hoylovendo.com considere como una oferta que carezca de seriedad o
        destinada a la burla.</span></p>

        <p  style='margin-left:.75in;text-align:justify;text-justify:
        inter-ideograph;text-indent:-.25in;mso-list:l1 level2 lfo5'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>y)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Y otros servicios, artículos,
        productos, etc, que sean penados o prohibidos por las leyes vigentes.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Si su anuncio infringe alguna de las prohibiciones establecidas e
        este documento, nos otorga la facultad para proceder a ejecutar alguna(s) de
        las siguientes sanciones:</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l4 level1 lfo6'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>a)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Cancelación del anuncio.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l4 level1 lfo6'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>b)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Limitación de los privilegios
        de la cuenta.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l4 level1 lfo6'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>c)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Suspensión de la cuenta.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l4 level1 lfo6'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>d)<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Remisión del caso a las
        autoridades correspondientes.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        ><o:p></o:p></span></b></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l4 level1 lfo6'>e) <b>Protecciónde de la Propiedad Intelectual.- </b>
        Los visitantes de hoylovendo.com y los usuarios titulares de
        derechos podrán identificar y solicitar la remoción de aquellos artículos que a
        su criterio infrinjan o violen sus derechos de Propiedad Intelectual.
        En caso que hoylovendo.com sospeche que se
        está cometiendo o se ha cometido una actividad ilícita o infractora de derechos
        de propiedad intelectual o industrial, se reserva el derecho de adoptar todas
        las medidas que entienda adecuadas, incluyendo la remoción del anuncio o
        publicidad que infrinja dichos derechos.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'>
        En caso de que el usuario viole algún derecho de propiedad
        intelectual de una persona natural o jurídica, queda en su total
        responsabilidad el resarcimiento del causado, quedando hoylovendo.com libre de
        cualquier responsabilidad jurídica por los anuncios, publicaciones que el
        usuario realice dentro de hoylovendo.com.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'>
        >Las marcas comerciales y las marcas mencionadas son propiedad de sus
        propietarios correspondientes. Cualquier titular de derecho, en particular los
        derechos de copyright, marca comercial u otros derechos terceros, pueden
        denunciar sobre cualquier anuncio de la página que vulnere sus derechos,
        informando, demostrando, la violación de sus derechos. Hoylovendo.com retirará
        el o los anuncios que sospeche violen estas condiciones.</p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l4 level1 lfo6'>f) Propiedad
        Intelectual. Enlaces.- Los contenidos de las pantallas relativas a los
        servicios de hoylovendo.com como así también los programas, bases de datos,
        redes, archivos que permiten el usuario acceder y usar su cuenta, son de
        propiedad de hoylovendo.com y están protegidas por las leyes y los tratados internacionales
        de derechos de autor, marca, patentes, modelos y diseños industriales.El uso indebido y la reproducción total o
        parcial de dichos contenidos quedan prohibidos, salvo autorización expresa y
        por escrito de hoylovendo.com.</p>

        <p  style='text-align:justify;text-justify:inter-ideograph'>>El sitio puede contener enlaces a otros sitios
        web lo cual no indica que sean propiedad u operados por hoylovendo.com. En
        virtud que hoylovendo.com no tiene control sobre tales sitios, NO será
        responsable por los contenidos, materiales, acciones y/o servicios prestados
        por los mismos, sean causadas directa o indirectamente.
        La presencia de enlaces a otros sitios no
        implica una sociedad, relación, aprobación, replicado de hoylovendo.com a dicho
        sitio y sus contenidos</p>


        <p  style='text-align:justify;text-justify:inter-ideograph'><b>
        SANCIONES, SUSPENSIÓN DE OPERACIONES, EXCLUSIÓN DE LOS USUARIOS DE
        RESPONSABILIDAD JURÍDICA A HOYLOVENDO.COM</b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Sin perjuicio de otras medidas, hoylovendo.com podrá advertir,
        suspender en forma temporal o inhabilitar definitivamente la cuenta de un
        usuario o una publicación, aplicar una sanción que impacte negativamente en la
        reputación de un usuario, iniciar una acción que estime pertinentes y/o
        suspender la prestación de servicios si:</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l5 level1 lfo7'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Se quebranta alguna ley, o
        cualquiera de las las estipulaciones de los términos y condiciones generales y
        demás políticas de hoylovendo.com;</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l5 level1 lfo7'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Si incumpliera sus compromisos
        como usuario;</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l5 level1 lfo7'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>3.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Si incurriera a criterio de
        hoylovendo.com en conductas o actos dolosos o fraudulentos;</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l5 level1 lfo7'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>4.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>No pudiera identificarse la
        identidad del usuario o cualquier información proporcionada por el mismo fuere
        errónea o ilegal;</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l5 level1 lfo7'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>5.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Hoylovendo.com entendiera que
        las publicaciones u otras acciones pueden ser causa de responsabilidad para el
        usuario que las publicó, para hoylovendo.com o para los usuarios.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l5 level1 lfo7'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>6.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>En el caso de la suspensión o
        inhabilitación de un usuario, todos los artículos que tuviera publicados y las
        ofertas realizadas también serán removidos del sistema.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Hoylovendo.com solo pone al disposición del usuario un espacio
        virtual gratuito que les permite ponerse en comunicación mediante internet para
        encontrar una forma de vender o comprar bienes y/o servicios. Hoylovendo.com
        como no es el propietario de los bienes y/o servicios ofrecidos, no tiene
        posesión de ellos ni los ofrece en venta.<span style='mso-spacerun:yes'> 
        </span>Hoylovendo.com no interviene en el perfeccionamiento de las operaciones
        realizadas entre los usuarios y/o visitantes al sitio de hoylovendo.com, ni en
        las condiciones realizadas entre los usuarios y/o visitantes del sitio, ni en
        las condiciones por ellos estipuladas para las mismas, por ello no será<span
        style='mso-spacerun:yes'>  </span>responsable respecto de la existencia,
        calidad, cantidad, estado, integridad o legitimidad de los bienes y/o servicios
        ofrecidos, adquiridos o enajenados por los usuarios, así como de la capacidad
        para contratar de los usuarios o de la veracidad de los datos personales por
        ellos ingresados.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >El usuario conoce y acepta que al realizar operaciones con otros
        usuarios o terceros lo hace bajo su propio riesgo.<span
        style='mso-spacerun:yes'>  </span>En ningún caso hoylovendo.com será
        responsable por lucro cesante, o por cualquier otro daño y/o perjuicio civil o
        penal o de cualquier naturaleza legal o moral que haya podido sufrir el
        usuario, debido a las operaciones realizadas o no realizadas por bienes y/o
        servicios publicados a través del servicio o sitio de hoylovendo.com.<b><o:p></o:p></b></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Se recomienda actuar con prudencia y sentido común el momento de
        realizar operaciones con otros usuarios. Usted como usuario debe tener presente
        los riesgos de contratar con menores o con personas que se valgan de una
        identidad falsa. Hoylovendo.com NO será responsable por la realización de
        ofertas y/o operaciones con otros usuarios basadas en la confianza depositada
        en el sistema o en el servicio brindados por hoylovendo.com en caso que uno o
        más usuarios o algún tercero inicien cualquier tipo de reclamo o acciones
        legales contra otro u otros usuarios, todos y cada uno de los usuarios
        involucrados en dichos reclamos o acciones eximen de toda responsabilidad a
        hoylovendo.com y su personal.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >En virtud que el usuario ofertante tiene la facultad para responder
        directamente a sus ofertantes se deja aclarado que, en ese caso, el usuario
        ofertante será el exclusivo responsable por esa decisión y las consecuencias
        que pudieran acarrear. Usted no podrá hacer responsable a hoylovendo.com, de
        los anuncios que se publiquen, oferten o compren, ya que NO estamos
        involucrados y no representamos a ninguna de las partes involucradas en la
        transacción real entre compradores, vendedor y/u ofertantes.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Toda vez que no controlamos ni garantizamos la calidad, la seguridad
        o la legalidad de los artículos y/o servicios puestos en oferta, la veracidad o
        la exactitud de los contenidos o los anuncios de los usuarios, la identidad de
        los mismos, la capacidad de los vendedores para vender artículos, la capacidad
        de los compradores para comprar y/o pagar los artículos y/o servicios puestos o
        el hecho de que los compradores y los vendedores vayan a completar realmente la
        transacción, hasta el máximo que permita la ley, excluimos todas las garantías,
        términos y conclusiones implícitos en cada transacción. No somos responsables
        ante cualquier pérdida de dinero, fondo de comercio o reputación, ni de
        cualquier daño especial, indirecto o consecuente que ocasione el uso que hagas
        de hoylovendo.com y sus servicio.<span style='mso-spacerun:yes'>  </span>Los
        titulares de registro propiedad intelectual que vean vulnerados sus derechos
        podrán iniciar las acciones de infracción de derechos de propiedad intelectual
        ante las instancias correspondientes directamente con el infractor excluyendo a
        hoylovendo.com de dichas acciones.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Si usted tiene una disputa con uno o varios usuarios, nos exime de
        responsabilidad (Tanto a nosotros como a nuestro personal) ante cualquier
        reclamación, querella, demanda y dalo (real o consecuente) de cualquier tipo o
        naturaleza, conocidos o no, que pueda surgir como consecuencia de tal disputa o
        esté relacionada con ella en forma alguna.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >ALCANCE DEL SERVICIOS<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Este acuerdo no crea ningún contrato de sociedad, de mandato, de
        franquicia, o relación laboral entre hoylovendo.com y el usuario. El usuario
        reconoce y acepta que hoylovendo.com no es parte en ninguna operación, ni tiene
        control alguno sobre la calidad, seguridad o legalidad de los artículos
        anunciados, la veracidad o exactitud de los anuncios, la capacidad de los
        usuarios para vender o comprar bienes y/o servicios.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Hoylovendo.com no puede asegurar que un usuario completará una
        operación ni podrá verificar la identidad o datos personales ingresados por sus
        usuarios. Hoylovendo.com no garantiza la veracidad de la publicación de
        terceros que aparezca en el sitio de hoylovendo.com y no será responsable por
        la correspondencia o contratos que el usuario celebre con dichos terceros o con
        otros usuarios.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >REPUTACIÓN DE USUARIOS<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Debido a que la verificación de la identidad de los usuarios en
        internet es difícil, hoylovendo.com no puede confirmar ni confirma, la
        identidad pretendida de cada usuario.<span style='mso-spacerun:yes'> 
        </span>Hoylovendo.com no tiene obligación de verificar la veracidad o exactitud
        de los mismos y NO se responsabiliza por los comentarios vertidos por cualquier
        usuario, por las ofertas de compra y venta que los usuarios realicen
        teniéndolos en cuenta o por la confianza depositada por cualquier otro comentario
        expresado dentro del sitio o atravesar de cualquier otro medio incluido el
        correo electrónico.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >MODIFICACIONES DEL ACUERDO<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Usted como usuario de hoylovendo.com, declara que otorga las
        facultades plenas a hoylovendo.com para modificar las condiciones de Uso del
        Servicio en cualquier momento, las cuales serán obligatorias, desde el momento
        en el que sean debidamente publicadas en el sitio.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l0 level1 lfo8'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>1.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Será responsabilidad del
        usuario el estar al tanto de las modificaciones o cambios que se realicen en el
        sitio; en caso de que no acepte los cambios a nuestras condiciones, puede
        cancelar el servicio utilizando o cancelar su cuenta.</span></p>

        <p  style='margin-left:.5in;text-align:justify;text-justify:inter-ideograph;
        text-indent:-.25in;mso-list:l0 level1 lfo8'><![if !supportLists]><span
        style='mso-fareast-font-family:"Liberation Serif";mso-bidi-font-family:
        "Liberation Serif"'><span style='mso-list:Ignore'>2.<span style='font:7.0pt "Times New Roman"'>
        </span></span></span><![endif]><span>Estas condiciones de uso
        constituyen la totalidad del acuerdo existente entre las partes acerca del
        asunto aquí tratado.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >INDEMNIZACIÓN<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >El usuario indemnizará y mantendrá indemne a hoylovendo.com, por
        cualquier reclamo o demanda de otros usuarios o terceros por sus actividades en
        el sitio o por su incumplimiento los términos y condiciones generales y demás
        políticas que se entienden incorporadas al presente o por la violación de
        cualesquiera leyes o derechos de terceros, incluyendo los honorarios de
        abogados de conformidad al arancel mínimo del Colegio de Abogados de Santa
        Cruz.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >DOMICILIO Y PROPIEDAD DEL SITIO<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Se fija como domicilio de hoylovendo.com la Calle Chichapi, S/N en
        Villa Bonita en la ciudad de Santa Cruz de la Sierra, Bolivia La página es de
        propiedad de Kublai Gómez Soto, ciudadano boliviano con CI 2363137LP</p>
        <p  style='text-align:justify;text-justify:inter-ideograph'><b><span
        >JURISDICCIÓN Y LEY APLICABLE<o:p></o:p></span></b></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        >Este acuerdo estará regido en todos sus puntos por las leyes
        vigentes en el Estado Plurinacional Bolivia. Cualquier controversia derivada
        del presente acuerdo, su existencia, validez, interpretación, alcance o
        cumplimiento, será sometida al arbitraje del Centro Empresarial de Mediación y
        Arbitraje de la Cámara de Industria y Comercio de Santa Cruz (CAINCO), de
        acuerdo con la reglamentación vigente de dicho tribunal. La sede de dicho
        arbitraje será la Ciudad de Santa Cruz de la Sierra.</span></p>

        <p  style='text-align:justify;text-justify:inter-ideograph'><span
        ><o:p></o:p></span></p>

        

    </div>

</div>

<script>
const app = new Vue({
    el: '#app',
});


</script>
@endsection
@extends('layouts._main')

@section('jstools')
@endsection

@include('partials.navbar1')
@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                @if ($mode =="create")
                    <div class="row" style="margin-top:1rem;">
                        <div class="col-6 border-blue" style="background-color:#3490dc; color:#fff;">
                            <span style="font-size:2rem">1</span> Crear el texto
                        </div>
                        <div class="col-6 border-blue" style="background-color:#fff; color:#ddd;">
                            <span style="font-size:2rem">2</span> Añadir fotos
                        </div>
                    </div>
                @else
                    <h2 class="border-blue">Editar anuncio</h2>
                @endif
                </div>
            </div>
            <div class="row justify-content-center top-1rem">
                <div class="col-md-12">
                    <form action={{url('/publishpost')}} method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="mode" value="{{$mode}}">
                        <input type="hidden" name="ad_id" value="{{$ad_id}}">
                        <div class="form-group">
                            <label for="título">Titulo:</label> 
                            <input id="título" name="título" type="text" class="form-control @error('title') is-invalid @enderror" value="{{($mode=='create'?old('título'):$ad->title)}}" placeholder="Qué es lo que quieres vender?">
                            @error('título')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="descripción">Descripción:</label> 
                            <textarea id="descripción" name="descripción" class="form-control @error('description') is-invalid @enderror" rows="4" placeholder="Detalla las características del producto o servicio que ofreces.">{{($mode=='create'?old('descripción'):$ad->description)}}</textarea>
                            @error('descripción')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="precio" style="display:block;">Precio </label> 
                            <input id="precio" name="precio" type="text" class="form-control col-4 @error('price') is-invalid @enderror" style="display:inline;" value="{{($mode=='create'?old('precio'):$ad->price)}}">
                            <select id="moneda" name="moneda" style="height: 2.2rem;">
                                <option value="Bs." {{($mode == 'create'?(old('moneda')=='Bs.'?'selected':''):($ad->currency=='Bs.'?'selected':''))}}  >Bolivianos (Bs.)</option>
                                <option value="$us." {{($mode == 'create'?(old('moneda')=='$us.'?'selected':''):($ad->currency=='$us.'?'selected':''))}} >Dólares ($us.)</option>
                            </select>
                            @error('precio')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="ciudad_id" style="display:block;">Departamento</label> 
                            <select id="ciudad_id" name="ciudad_id" style="height: 2.2rem;">
                            <option value="0"></option>
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}" {{($mode=='create'? (old('ciudad_id') == $city->id?'selected':'') : ($city->id==$ad->city_id ? 'selected':''))}} >{{$city->name}}</option>
                                @endforeach
                            </select>
                            @error('ciudad_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="categoria_id" style="display:block;">Categoría</label> 
                            <select id="categoria_id" name="categoria_id" style="height: 2.2rem;">
                            <option value="0"></option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{($mode=='create'?(old('categoria_id') == $category->id?'selected':''):($category->id==$ad->category_id?'selected':''))}} >{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('categoria_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="teléfono" style="display:block;">Teléfono: </label> 
                            <input id="teléfono" name="teléfono" type="teléfono" class="form-control  @error('phone') is-invalid @enderror" style="display:inline;" value="{{($mode=='create'?old('teléfono'):$ad->phone)}}" placeholder="A qué número quieres que te llamen">
                            @error('teléfono')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group" style="padding-top:0.9rem;">
                            <button type="submit" id="formsubmit" class="btn btn-primary">{{($mode=='create'?"Paso 2 (Fotos)":"Guardar Anuncio")}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</div>

<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
});
</script>
@endsection


@extends('layouts._main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="border-blue">Login</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center top-06rem">
                    <img src="/img/login.png" class="img-fluid"/>
                    <p>Hola, accede usando usando Facebook o Gmail.</p>
                </div>
            </div>

            <div class="row justify-content-center">
                    <a href="/login/facebook">
                        <img src="/img/bg-login-facebook.png" style="width:250px; margin:auto; display:block; margin:10px;">
                    </a>
                    <a href="/login/google">
                        <img src="/img/bg-login-google.png" style="width:250px; margin:auto; display:block; margin:10px;">
                    </a>
            </div>

        </div>

        
    </div>
</div>
@endsection

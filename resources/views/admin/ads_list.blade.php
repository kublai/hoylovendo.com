@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')
<div class="container" id="app">
    <div class="row">
        <div class="col-md-12 top-1rem">
            <h3 class="border-blue">Listado de anuncios</h3>
        </div>
    </div>
    @foreach ($ads as $ad)
        <div class="row" style="border-bottom: 1px solid #ccc; margin-bottom:1rem;"> 
            <div class="col-12 col-md-6" >
                    id: {{$ad->id}}<br>
                    user Id: {{$ad->user_id}}<br>
                    title: {{$ad->title}}<br>
                    description: {{$ad->description}}<br>
                    price: {{$ad->price}} {{$ad->currency}}<br>
                    phone: {{$ad->phone}}<br>
                    category: {{$ad->category->name}}<br>
                    city: {{$ad->city->name}}<br>
                    
            </div>
            <div class="col-12 col-md-6">
                status: {{$ad->status}}<br>
                status_message: {{$ad->message}}<br>
                sold: {{$ad->sold}}<br>
                views: {{$ad->views}}<br>
                boost: {{$ad->boost}}<br>
                created_at: {{$ad->created_at}}<br>
                updated_at: {{$ad->updated_at}}
            </div>
            <div class="col-12 " style="padding:1rem; text-align:right;">
            <a href="{{route('admin_edit_ad',['ad_id'=>$ad->id])}}" class="btn btn-primary">Edit</a>
            <a href="#" class="btn btn-primary" @click="deleteAd($event,{{$ad->id}})">Delete</a>
            </div>
            
        </div>
    @endforeach

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
    data:{
        aux: 0
    },
    methods:{
        deleteAd: function(event,ad_id){
            event.preventDefault();
            swal({
                title: '¿Eliminar anuncio?',
                icon: 'warning',
                buttons:{ accept:'Si', cancel:'No' }
                }).then((result) => {
                    if (result == 'accept'){
                        axios('/admin/ads/delete/' + ad_id ).then((response)=>{
                            if (response.data.result === 'ok'){
                                swal("Anuncio Eliminado, recargar pagina para actualizar datos");
                            }else{
                                swal("Upsss, algo ha salido mal... inténtalo más tarde");
                            }                            
                        });  
                    }
                });
            
        }
    }
});
</script>
@endsection
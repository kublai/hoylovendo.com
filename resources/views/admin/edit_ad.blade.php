@extends('layouts._main')

@section('jstools')
@endsection

@include('partials.navbar1')
@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="border-blue">Editar anuncio (Admin)</h2>
                </div>
            </div>
            <div class="row justify-content-center top-1rem">
                <div class="col-md-12">
                    <form action={{url('/admin/ads/post')}} method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="ad_id">ID:</label> 
                            <input type="text" id="ad_id" name="ad_id"   class="form-control @error('ad_id') is-invalid @enderror" value="{{$ad->id}}" >
                            @error('ad_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="título">Titulo:</label> 
                            <input id="título" name="título" type="text" class="form-control @error('title') is-invalid @enderror" value="{{$ad->title}}">
                            @error('título')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="descripción">Descripción:</label> 
                            <textarea id="descripción" name="descripción" class="form-control @error('description') is-invalid @enderror" rows="4" placeholder="Detalla las características del producto o servicio que ofreces.">{{$ad->description}}</textarea>
                            @error('descripción')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="precio" style="display:block;">Precio</label> 
                            <input id="precio" name="precio" type="text" class="form-control col-4 @error('price') is-invalid @enderror" style="display:inline;" value="{{$ad->price}}">
                            <select id="moneda" name="moneda" style="height: 2.2rem;">
                                <option value="Bs." {{$ad->currency == 'Bs.' ? 'selected' : ''}}>Bolivianos (Bs.)</option>
                                <option value="$us." {{$ad->currency == '$us.' ? 'selected' :''}}>Dólares ($us.)</option>
                            </select>
                            @error('precio')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="ciudad_id" style="display:block;">Departamento</label> 
                            <select id="ciudad_id" name="ciudad_id" style="height: 2.2rem;">
                            <option value="0"></option>
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}" {{$city->id==$ad->city_id ? 'selected':''}} >{{$city->name}}</option>
                                @endforeach
                            </select>
                            @error('ciudad_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="categoria_id" style="display:block;">Categoría</label> 
                            <select id="categoria_id" name="categoria_id" style="height: 2.2rem;">
                            <option value="0"></option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" {{$category->id==$ad->category_id?'selected':''}} >{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('categoria_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="teléfono" style="display:block;">Teléfono: </label> 
                            <input id="teléfono" name="teléfono" type="text" class="form-control  @error('phone') is-invalid @enderror" style="display:inline;" value="{{$ad->phone}}" >
                            @error('teléfono')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <hr>
                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="user_id" style="display:block;">User Id: </label> 
                            <input id="user_id" name="user_id" type="text" class="form-control  @error('user_id') is-invalid @enderror" style="display:inline;" value="{{$ad->user_id}}" >
                            @error('user_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="status" style="display:block;">Status: 0 created (User) | 1 Published (Admin) | 2 Rejected with Message (Admin) | 3 Revision Requested (User)</label> 
                            <input id="status" name="status" type="text" class="form-control  @error('status') is-invalid @enderror" style="display:inline;" value="{{$ad->status}}" >
                            @error('status')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group" style="padding-top:0.9rem;">
                            <label for="message" style="display:block;">Message to the user (Why the ad is not published?)</label> 
                            <input id="message" name="message" type="text" class="form-control  @error('message') is-invalid @enderror" style="display:inline;" value="{{$ad->message}}" >
                            @error('message')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <hr>
                        <div class="form-group" style="padding-top:0.9rem;">
                            <button type="submit" id="formsubmit" class="btn btn-primary">Guardar Anuncio</button>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>
</div>

<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
});
</script>
@endsection


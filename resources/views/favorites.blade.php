@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')
<div class="container" id="app">
    <div class="col-md-12 top-1rem">
        <h3 class="border-blue">Mis favoritos</h3>
        <div v-if="sizeCardsComputed>0" id="cardsContainer" class="row">
            <ad-card v-for="(ad, index) in cards" :ad="ad" :index="index" :key="index" alertparentdelete="true" @remove="removeCard">
            </ad-card>
        </div>
        <div v-else class="text-center" :style="{display:emptyListComputed}">
            <img class="img-fluid" src="/img/noitems.png">
            <p>No tienes ningún anuncio en tu lista de favoritos</p>
            <a href="{{route('home')}}" class="btn btn-primary">Ver últimos anuncios</a>
        </div>
    </div>
    <div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
    data:{
        itemsPerPage:{{$items_per_page}},
        cards: {},
        pagecounter: 0
    },
    methods:{
        submitSearch: function(event){
            this.$refs.searchform.submit();
        },
        loadCards: function(){
            this.pagecounter++;
            axios('/getcardsdatafavs/' + this.itemsPerPage + '/' + this.pagecounter).then((response)=>{
                this.cards = response.data;
            });
        },
        removeCard: function(cardId){
            this.cards.forEach((item,index) => {
                if (item.id == cardId){
                    this.cards.splice(index,1);
                    return;
                }
            });
        }

    },
    computed:{
        sizeCardsComputed: function(){
            return this.cards.length;
        },
        emptyListComputed: function(){
            return (this.cards.length == 0) ? 'block' : 'none';
        }
    },
    mounted: function(){
        this.loadCards();
    }
});
</script>
@endsection
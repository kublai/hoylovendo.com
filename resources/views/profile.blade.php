@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')
<div class="container" id="app">
    <div class="row">
        <div class="col-md-12 top-1rem">
            <h3 class="border-blue">Mi Perfil</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4">
            <img class="img-fluid" src="{{$profile->image}}" width="100%">
        </div>
        <div class="col-12 col-md-8">
            <div class="form-group">
                <label for="nombre">Nombre:</label> 
                <input id="nombre" name="nombre" type="text" class="form-control" value="{{$profile->name}}" disabled>
            </div>
            <div class="form-group">
                <label for="proveedor">Cuenta creada con:</label> 
                <input id="proveedor" name="proveedor" type="text" class="form-control" value="{{$profile->provider}} " disabled>
            </div>
            <div class="form-group">
                <label for="fecha_creacion">Fecha de alta:</label> 
                <input id="fecha_creacion" name="fecha_creacion" type="text" class="form-control" value="{{$profile->created_at}}" disabled>
            </div>
            <div class="form-group">
                <label for="number_ads">Nro de anuncios:</label> 
                <input id="number_ads" name="number_ads" type="text" class="form-control" value="{{$number_ads}}" disabled>
            </div>
            <div class="form-group">
                <label for="total_views">Total visualizaciones:</label> 
                <input id="total_views" name="total_views" type="text" class="form-control" value="{{$total_views}}" disabled>
            </div>
        </div>
    </div>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
    data:{

    },
    methods:{
        submitSearch: function(event){
            this.$refs.searchform.submit();
        },

    },
    computed:{
        sizeCardsComputed: function(){
            return this.cards.length;
        },
        emptyListComputed: function(){
            return (this.cards.length == 0) ? 'block' : 'none';
        }
    },
    mounted: function(){
    }
});
</script>
@endsection
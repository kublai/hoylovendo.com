@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-8 col-lg-offset-2">
            <h1 class="text-center">Compra y vende todo lo que quieras</h1>
            <p class="text-center">Encuentra fácilmente todo lo que necesitas a muy buen precio y en tu ciudad.
            Recuerda, si tienes algo para vender <a class="growtext" href="/signup">publica aquí tu anuncio</a>. </p>
            <form method="POST" action="{{ route('searchresults') }}">
                @csrf
                <input type="hidden" name="categoryfilter" value="0">
                <input type="hidden" name="cityfilter" value="0">
                <div class="form-group row">
                    <div class="col-md-10">
                        <input id="searchtext" class="form-control" style="border-radius:25px; margin:10px 0px" type="text"  name="searchtext" value="{{ old('search') }}" required placeholder="{{__('I´m looking for...')}}">
                        @error('searchtext')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary" style="width:100%; border-radius:25px; margin:10px 0px;">
                            {{ __('Search') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center" id="categories-container">
        <div class="col-3 col-md-2">
            <a href="/" class="text-center category-icon {{$cat_id == 0 ? 'selected-category' : ''}}" style="cursor:pointer;">
                <img src="/img/icons/todas-las-categorias.png" class="category-icon-img" alt="todas las categorias">
                <span class="label-cat" >Todas la categorías</span>
            </a>
        </div>
        @foreach($categories as $cat)
        <div class="col-3 col-md-2">
            <a href="/categoria/{{$cat->id}}" class="text-center category-icon {{$cat_id == $cat->id ? 'selected-category' : '' }}" style="cursor:pointer;">
                <img src="/img/icons/{{str_replace(' ','_',$cat->name)}}.png" class="category-icon-img" alt="{{$cat->name}}">
                <span class="label-cat" >{{$cat->name}}</span>
            </a>
        </div>
        @endforeach
    </div>
    <div class="col-md-12 top-1rem">
        @if($cat_selected == '')
            <h4 class="border-blue">Últimos anuncios publicados</h4>
        @else
            <h4 class="border-blue">Últimos anuncios "{{$cat_selected}}"</h4>
        @endif
        <!-- <div class="row">
            <div class="col-12">
                <ins class="adsbygoogle"
                    style="display:block"
                    data-ad-client="ca-pub-9967014715142224"
                    data-ad-slot="8884822185"
                    data-ad-format="auto"
                    data-full-width-responsive="true"></ins>
            </div> 
        </div> -->
        <div id="cardsContainer" class="row">
            <ad-card v-for="(ad, index) in cards" :ad="ad" :index="index" :key="index">
            </ad-card>
        </div>
    </div>
    <div >
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div id="loadMore" class="text-center" style="padding:20px;">
        <button id="btn-loadmore" class="btn btn-primary" @click="loadCards">Ver más anuncios</button>
    </div>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9967014715142224"
     crossorigin="anonymous"></script>
    <!-- homepage banner -->
    <ins class="adsbygoogle"
        style="display:block"
        data-ad-client="ca-pub-9967014715142224"
        data-ad-slot="5234889501"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>

<script>
const app = new Vue({
    el: '#app',
    data:{
        itemsPerPage:{{$items_per_page}},
        cards: {},
        pagecounter: 0,
        catFilterId: {{$cat_id}}
    },
    methods:{ 
        loadCards: function(){
            this.pagecounter++;
            axios('/getcardsdata/' + this.itemsPerPage + '/' + this.pagecounter + '/' + this.catFilterId).then((response)=>{
                this.cards = response.data;
            });
        },
        seachByCategory: function(categoryId){
            this.catFilterId = categoryId;
            this.pagecounter = 1;
            axios('/getcardsdata/' + this.itemsPerPage + '/' + this.pagecounter + '/' + this.catFilterId).then((response)=>{
                this.cards = response.data;
            });            
        } 
    },
    mounted: function(){
        this.loadCards();
    }
});

if({{$cat_id}} > 0){
    document.addEventListener('DOMContentLoaded', function(event) {
        document.getElementById('categories-container').scrollIntoView();
        //if there is no category selected focus goes to the search box
    })   
}  

//adsense code 
(adsbygoogle = window.adsbygoogle || []).push({});

</script>
@endsection
@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')

<div class="container" id="app">
    <form id="searchForm" ref="searchform" method="POST" action="{{ route('searchresults') }}">
        @csrf
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 col-lg-offset-2">   
                <div class="form-group row">
                    <div class="col-md-10">
                        <input v-model="searchtext" id="searchtext" class="form-control" 
                            style="border-radius:25px; margin:10px 0px" 
                            type="text"  name="searchtext"
                            required 
                            autofocus>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary" style="width:100%; border-radius:25px; margin:10px 0px;">
                            {{ __('Search') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <span class="hide-small" style="line-height: 1.2rem; padding-top: 0.5rem;">Categoria</span>
            <select class="filter-select" id="categoryfilter" name="categoryfilter" @change="submitSearch($event)" style="padding:5px 0px;">
                <option value="0" >Todas</option>
                @foreach($categories as $cat)
                    <option value="{{$cat->id}}" {{$cat->id == $filter_category ? 'selected': ''}}>
                        {{$cat->name}} 
                    </option>
                @endforeach
            </select>

            <span class="hide-small" style="line-height: 1.2rem; padding-top: 0.5rem;">en</span>
            <select class="filter-select" id="cityfilter" name="cityfilter" @change="submitSearch($event)" style="padding:5px 0px;">
                <option value="0" >Bolivia</option>
                @foreach($cities as $city)
                    <option value="{{$city->id}}" {{$city->id == $filter_city ? 'selected': ''}} >
                        {{$city->name}} 
                    </option>
                @endforeach
            </select>
        </div>
    </form>
    <div class="col-md-12 top-1rem">
        <h3 class="border-blue">Resultados</h3>
        <div id="cardsContainer" class="row">
            <ad-card v-for="(ad, index) in cards" :ad="ad" :index="index" :key="index">
            </ad-card>
        </div>

    </div>
    <div >
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
    </div>
</div>
<script>
const add= new Vue({
    el: "#app",
    data:{
        itemsPerPage:{{$items_per_page}},
        cards: {},
        pagecounter: 0,
        searchtext: "{{$searchtext}}",
        catFilterId : "{{$filter_category}}",
        cityFilterId : "{{$filter_city}}"
    },
    props:[],
    methods:{
        submitSearch: function(event){
            this.$refs.searchform.submit();
        },
        loadCards: function(){
            this.pagecounter++;
            axios('/getcardsdata/' + this.itemsPerPage + '/' + this.pagecounter + '/' + this.catFilterId + '/'+ this.cityFilterId +'/' + this.searchtext).then((response)=>{
                this.cards = response.data;
            });
        },

    },
    mounted: function(){
        this.loadCards();
    }
});
</script>

@endsection
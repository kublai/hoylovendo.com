@extends('layouts._main')

@section('jstools')

@endsection

@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="border-blue">Publicar un anuncio</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center top-06rem">
                    <img src="/img/welcome.jpg" class="img-fluid"/>
                    <p>Bienvenido a <b>hoylovendo.com</b>, puedes crear tu cuenta usando Facebook o Gmail.</p>
                <div class="col-md-12">
            </div>

            <div class="row justify-content-center">
                    <a href="/login/facebook">
                        <img src="/img/bg-login-facebook.png" style="width:250px; margin:auto; display:block; margin:10px;">
                    </a>
                    <a href="/login/google">
                        <img src="/img/bg-login-google.png" style="width:250px; margin:auto; display:block; margin:10px;">
                    </a>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
});
</script>
@endsection


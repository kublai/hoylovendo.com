@extends('layouts._main')

@section('jstools')
@endsection

@section('content')
@include('partials.navbar1')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12 ">
                    <h2 class="border-blue">Mis anuncios</h2>
                </div>
                <div class="col-md-12">
                @if (sizeof($ads) > 0)
                    <a href="{{route('publish')}}" class="btn btn-primary float-right">Crear anuncio</a>
                @endif
                </div>
            </div>
            @if (sizeof($ads)==0)
            <div class="row">
                <div class="col-md-12 text-center">
                <a class="img-responsive" href="{{route('publish')}}"><img src="/img/empezar.png" alt="empezar a crear anuncios"></a>
                    <p style="padding-top:20px;">Aun no tienes anuncios, es un buen momento para crear tu primer anuncio.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                <a class="btn btn-primary" href="{{route('publish')}}">Publicar anuncio</a>
                </div>
            </div>
            @else
                @foreach ($ads as $ad)
                    <ad-item ad="{{json_encode($ad)}}" photo="{{json_encode($ad->mainPhoto)}}" category="{{$ad->category->name}}" city="{{$ad->city->name}}"></ad-item>
                @endforeach
            @endif
        </div>
    </div>
</div>
<script>
const app = new Vue({
    el: '#app'
});


</script>
@endsection

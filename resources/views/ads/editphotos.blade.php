@extends('layouts._main')

@section('jstools')
@endsection

@include('partials.navbar1')
@section('content')
<div class="container" id="app">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div id="app" class="row">
                <div class="col-md-12">    
                    <h2 class="border-blue">Fotos</h2>
                    <a onclick="window.location.href ='{{route('dashboard')}}'" class="btn btn-primary float-left" style="color:#FFF;">Mis Anuncios</a>
                    <a href="{{route('addphoto',['ad_id'=>$ad->id])}}" class="btn btn-primary float-right" >Añadir foto</a>
                </div>
                 <div class="col-md-12">
                    <p class="border-grey" style="margin-bottom: 1px; padding-bottom: 6px;">{{$ad->title}}</p>
                 </div>
            </div>
                <photos-list photos="{{json_encode($photos)}}" ></photos-list>
        </div>

    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
});
</script>
@endsection

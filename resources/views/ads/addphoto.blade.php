@extends('layouts._main')

@section('jstools')
@endsection

@include('partials.navbar1')

@section('content')
<div id="app">
    <div id="loader" style="display:none; position:absolute; top:0px; left:0px; background-color:#fff; width:100%; height:100%; z-index:100; opacity:1;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 text-center">
                    <h4 style="margin-top:1rem;">Procesando imagen...</h4>
                    <img class="img-fluid" src="/img/uploading-photos.png">
                    <p>Subiendo imagen al servidor</p>
                </div>
                <div class="col-12 col-md-8" style="height: 20px; margin-top:1rem;">
                    <div class="progress" style="height:20px">
                        <div id="progress1" class="progress-bar" role="progressbar" style="width: 0%; height:20px" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>   
                    </div>                   
                </div>
                <div v-if="inprocessComputed" class="col-12 col-md-8 text-center " style="margin-top:1rem;">
                    <p>Hecho, ¡Imagen añadida al anuncio!</p>
                    <button class="btn btn-success" @click="finishOk">Volver al listado de anuncios</button>
                    
                </div>
                <div v-else class="col-12 col-md-8 text-center " style="margin-top:1rem;">
                    <p>Ten paciencia, este proceso puede llegar a tardar minutos dependiendo del tamaño de la fotografía y tu velocidad de tu conexión.</p>
                    <button class="btn btn-danger" @click="cancelUpload">Cancelar proceso</button>
                </div>
                
            </div>
        </div>
    </div>
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div id="app" class="row">
                    <div class="col-md-12">    
                        <h2 class="border-blue">Fotos</h2>
                        <p>Formatos permitidos: jpg, png</p>
                        <form action="#" method="POST" enctype="multipart/form-data" @submit="submitImages">
                            @csrf
                            <input type="hidden" id="ad_id" name="ad_id" value="{{$ad_id}}">
                            <div class="form-group" style="padding-top:0.6rem;"> 
                            <input type="file" class="form-control-file" id="foto1" name="foto1"  style="margin-bottom:15px; padding:4px;" value="{{old('foto1')}}">
                                @error('foto1')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group text-center" style="padding-top:0.9rem;">
                                <button type="submit" id="formsubmit" class="btn btn-primary">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript"> 
const app = new Vue({
    el: "#app",
    data:{
        imgCounter: 0,
        imgCounterCtrl:0,
    },
    computed:{
        inprocessComputed: function(){
            if(this.imgCounter == this.imgCounterCtrl){
                return true;
            }else{
                return false;
            }
        },
    },
    methods:{
        cancelUpload: function(){
            location.reload();
        },
        finishOk: function(){
            window.location.href ="/dashboard";
        },
        submitImages: function(event){
            event.preventDefault();
            window.scrollTo(0,0);
            document.getElementById('loader').style.display="inline";

            f1 = document.getElementById('foto1');
            var progress1 = document.getElementById("progress1");
            var _this = this;

            var config1 = {
                onUploadProgress: function(progressEvent) {
                var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                progress1.setAttribute('aria-valuenow', percentCompleted);
                progress1.style.width=percentCompleted+"%";
                }
            };         
            if (f1.files.length>0){
                this.imgCounter++;
                var data1 = new FormData();
                data1.append('ad_id',{{$ad_id}});
                data1.append('foto', f1.files[0]);
                data1.append('main', 1);
                data1.append('counter', 1);
                axios.post("{{url('/publishpostphotos')}}", data1, config1)
                    .then(function (res) {
                        progress1.innerHTML = "Imágen cargada correctamente";
                        _this.imgCounterCtrl++;
                    })
                    .catch(function (err) {
                        progress1.className="progress-bar bg-danger";
                        progress1.innerHTML = "Ups.., ERROR al subir esta imágen";
                        _this.imgCounterCtrl++;
                    });
            }
        }
    }
});
</script>
@endsection

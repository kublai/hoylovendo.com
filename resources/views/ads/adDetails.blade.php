@extends('layouts._main')

@section('jstools')
@endsection

@include('partials.navbar1')

@section('content')
<div class="container" id="app">
    <div class="row">
        <div class="col-md-12 top-1rem">
            <h3 class="border-blue">{{$ad->title}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 " style="">
            <div id="carouselControls" class="carousel slide carousel-max-height" data-ride="carousel" style="">
            <ol class="carousel-indicators">
                @foreach($photos as $photo)
                    <li data-target="#carouselControls" data-slide-to="{{$counter++}}" class="{{$photo->main == 1 ? 'active':''}}"></li>
                @endforeach
            </ol>
            <img class="sold-big" src="/img/vendido.png" style="z-index:1; position:absolute; top:0px; left:0px; display: {{ is_null($ad->sold)?'none':'' }} ">
                <div class="carousel-inner">
                    @foreach($photos as $photo)
                        <div class="carousel-item text-center {{$photo->main == 1 ? 'active':''}} carousel-max-height">
                            <img class="carousel-max-height" src="/storage/thumb800px/{{$photo->file}}" alt="{{$ad->title}}"  >
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-md-4 " style="">
            <div class="" style="padding:0.7rem;" >
                <H4><b>{{$ad->price}} {{$ad->currency}} </b><h4>
                <h4 class="" style="overflow-y: hidden; height:1.4rem;">{{$ad->title}}</h4>
                <h5>Telf: {{$ad->phone}}</h5> 
                <div style="display:Block">
                    <h6>{{$ad->city->name}} / {{$ad->category->name}}</h6>
                </div>
                <div class="card-text" >
                    <p style="min-height: 70px;">{{$ad->description}}</p>
                    
                    <div style="display: inLine-flex">
                    <img src="{{$ad->user->image}}" width="50px" height="50px" style="-webkit-border-radius: 5px; border-radius: 5px;">
                    </div>
                    <div style="font-size:12px; display:inLine-Block">
                    Visto: {{$ad->views}} veces.<br>
                    Usuario: {{$ad->user->name}}<br>
                    Publicado: {{$ad->created_at}}</div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('index');
Route::get('/categoria/{cat_id}', 'HomeController@index')->name('indexwithcat');
Auth::routes();

Route::get('signup','SignupController@signup')->name('signup'); // Signup, first time enterging the system
Route::get('logout','SignupController@signup')->name('logout'); // logout, is route is called when the session expires and the user was logged

Route::get('dashboard', 'PublishController@dashboard')->name('dashboard'); //list of ads
Route::get('publish', 'PublishController@publish')->name('publish'); // create a new ad
Route::post('publishpost','PublishController@publishPost')->name('publishpost'); //Receive the Data validate and save ad in the DDBB
Route::get('publish2/{ad_id}/{mode}','PublishController@publish2')->name('publish2'); // show the form to add photos to an add,
Route::post('publishpostphotos','PublishController@publishPostPhotos')->name('publishpostphotos'); //Receive photo files validate and save files

Route::get('dashboard/itemsold/{ad_id}','DashboardController@markAsSold')->name('dashboard_itemsold'); //Mark an item as sold
Route::get('dashboard/deletead/{ad_id}','DashboardController@deleteAd')->name('dashboard_deletead'); //delete an ad from the database

Route::get('editad/{ad_id}','PublishController@editad')->name('editad'); //edit an ad from the dashboard
Route::get('editphotos/{ad_id}','PublishController@editphotos')->name('editphotos'); //show the list of photos for an ad
Route::delete('deletephoto/{ad_id}','PublishController@deletephoto')->name('deletephoto'); //delete a photo (via ajax)
Route::get('addphoto/{ad_id}','PublishController@addPhoto')->name('addphoto'); // add a new photo to an ad
Route::post('addphotopost','PublishController@addPhotoPost')->name('addphotopost'); // add a new photo to an ad - post form processing
Route::get('rotatephoto/{photo_id}','PhotoController@rotatePhoto')->name('rotatephoto'); //AXIOS call to rotate a photo
Route::get('setasmainphoto/{photo_id}','PhotoController@setAsmMainPhoto')->name('setasmainphoto'); //AXIOS set the imgage as main photo for an ad

Route::get('login/{provider}', 'Auth\SocialiteLoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\SocialiteLoginController@handleProviderCallback');
Route::get('/perfil','Auth\ProfileController@index')->name('perfil');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/searchresults', 'HomeController@searchResults')->name('searchresults');
Route::post('/searchresults', 'HomeController@searchResults')->name('searchresults');
Route::get('/anuncio/{category}/{slug}','HomeController@adDetails')->name('anuncio');

Route::get('/tagfav/{ad_id}','FavoriteController@tagFavorite');
Route::get('/tagfav/{ad_id}/{current_url}','FavoriteController@tagFavorite')->name('tagfav');
Route::get('/favoritos','FavoriteController@index')->name('favoritos');

Route::get('/getcardsdata/{itemsperpage}/{pagenumber}/{catFilterId}','HomeController@getCardsData')->name('getcardsdata');
Route::get('/getcardsdata/{itemsperpage}/{pagenumber}/{catFilterId}/{cityFilterId}/{searchtext}','HomeController@getCardsData')->name('getcardsdatasearch');
Route::get('/getcardsdatafavs/{itemsperpage}/{pagenumber}','FavoriteController@getCardsDataFavs')->name('getcardsdatafavs');

//Route::get('login/google', 'Auth\SocialiteGoogleLoginController@redirectToProvider');
//Route::get('login/google/callback', 'Auth\SocialiteGoogleLoginController@handleProviderCallback');

Route::get('/terminos-y-condiciones', 'ContentController@terms')->name('terminos');
Route::get('/politica-de-privacidad', 'ContentController@privacy')->name('privacidad');

Route::get('/ping', 'SolariumController@ping');

//ADMIN SUBSYSTEM
Route::get('/admin/ads','AdminController@adsList')->name('admin_ads_list');
Route::get('/admin/ads/edit/{ad_id}','AdminController@editAd')->name('admin_edit_ad');
Route::post('/admin/ads/post','AdminController@postAd')->name('admin_post_ad');
Route::get('/admin/ads/delete/{ad_id}','AdminController@deleteAd')->name('admin_delete_ad');


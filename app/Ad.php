<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'ads';

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function mainPhoto()
    {
        return $this->hasone('App\Photo')->where('main','=', 1);
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * return the register of the favorite status for an ad for the logged used
     * 
     */
    public function favorite()
    {
        if($user = auth()->user()){
            return $this->hasone('App\Favorite')->where("user_id", "=", auth()->user()->id);
        }else{
            return $this->hasone('App\Favorite')->where("user_id", "=", 0);
        }
    }

    public function getSlugAttribute(){
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $this->title);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text ."-". $this->id;
    }
}

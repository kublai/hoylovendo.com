<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';

    public function ad(){
        return $this->belongsTo('App\Ad');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
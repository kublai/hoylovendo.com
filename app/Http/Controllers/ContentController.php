<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use App\City;
use App\Photo;
use App\Favorite;

use Illuminate\Http\Request;
use App\Http\PhotoUtilities;

class ContentController extends Controller
{
    const ITEMS_PER_PAGE = 16;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * política de privacidad
     * 
     * @return view
     */

    public function terms(){
        $title="Términos y condiciones de uso de hoylovendo.com";
        return view('terms', compact('title'));
    }

}

<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use App\City;
use App\Photo;

use Illuminate\Http\Request;
use App\Http\PhotoUtilities;

class AxiosController extends Controller
{

    /**
     * return a JSON structure with the data for all the cards requested
     * 
     * @param integer $items_per_page
     * @param integer $page_number 
     * @param integer $cat_id
     * 
     * @return string Json encoded string
     */
    public function cards($items_per_page, $page_number, $cat_id = 0){
        $items_per_page = intval($items_per_page);
        $page_number = intval($page_number);
        $cat_id = intval($cat_id);

        $cat_selected = '';
        $categories = Category::all();
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium')); 
        $query = $client->createSelect();
        $query->setQuery('*:*');
        $start_item = ($page_number-1)*$items_per_page;
        $query->setStart($start_item)->setRows($items_per_page);
        $query->setFields(array('id'));
        if ($cat_id>0){
            $filter_category = Category::where('id',$cat_id)->first();
            $query->createFilterQuery('category')->setQuery("category:\"" . $filter_category->name ."\"");
            $cat_selected = $filter_category->name;
        }
        $query->addSort('created_at', $query::SORT_DESC);
        $resultset = $client->select($query);
        $ids =[];
        foreach($resultset as $document){
            foreach($document as $field=>$value){
                if ($field == "id"){
                    array_push($ids, $value);
                }  
            }
        }
        $unordered_ads = Ad::whereIn('id',$ids)->get();
        $ads = $unordered_ads->sortBy(function($model) use ($ids){
            return  array_search($model->getKey(), $ids);  //search the id of the model in the list of correct ordered results returned by Solr
        });
        return json_encode($ads);
    }
}
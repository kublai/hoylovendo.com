<?php

namespace App\Http\Controllers;

use App\Ad;
use App\User;
use App\City;
use App\Category;
use App\Favorite;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Redirect;

class AdminController extends Controller
{
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = auth()->user();
    }

    /**
     * List of ads
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adsList()
    {
        $adminuser = User::find(auth()->user()->id);
        if ( is_null($adminuser) || $adminuser->type == 0){
            echo "Acceso denegado";die;
        }
        $ads = Ad::all();
        return view('admin.ads_list',compact('ads'));
    }

    /*+
    * edit ad
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function editAd($ad_id)
    {
        $ad = Ad::find($ad_id);
        if(is_null($ad)){
            echo "Ad not found"; die;
        }
        $cities = City::all();
        $categories = Category::all();
        return view('admin/edit_ad',compact('ad','cities','categories'));
    }

    /*+
    * post ad
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function postAd(Request $request)
    {
    
        $ad_id = $request->ad_id;
        $user_id = $request->user_id;
        $title = $request->título;
        $description = $request->descripción;
        $price = $request->precio;
        $currency = $request->moneda;
        $phone = $request->teléfono;
        $category_id = $request->categoria_id;
        $city_id = $request->ciudad_id;

        if(is_null($ad_id)){
            echo "Ad $ad_id not found"; die;
        }
        $status = intval($request->status);
        $message = $request->message;
        $ad = Ad::find($ad_id);
        $ad->status = $status;
        $ad->message = $message;
        $ad->user_id = $user_id;
        $ad->title = $title;
        $ad->description = $description;
        $ad->price = $price;
        $ad->currency = $currency;
        $ad->city_id = $city_id;
        $ad->category_id = $category_id;
        $ad->phone = $phone;
        $ad->save();

        // Create or update the register in SOLR
        // create a client instance
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
        $update = $client->createUpdate();
        
        // get an update query instance
        $createdAux = new \DateTime($ad->created_at);
        $dateTimeCreated = $createdAux->format("Y-m-d") . "T" . $createdAux->format("G:i:s") . "Z";
        $boostAux = new \DateTime($ad->boost);
        $dateTimeBoost = $boostAux->format("Y-m-d") . "T" . $boostAux->format("G:i:s") . "Z";
        $updatedAux = new \DateTime($ad->updated_at);
        $dateTimeUpdated = $updatedAux->format("Y-m-d") . "T" . $updatedAux->format("G:i:s") . "Z";
        
        // create a new document for the data
        $doc = $update->createDocument();
        $doc->id = $ad->id;
        $doc->user_id = $ad->user_id;
        $doc->title = $ad->title;
        $doc->description = $ad->description;
        $doc->price = $ad->price;
        $doc->currency = $ad->currency;
        $doc->phone = $ad->phone;
        $doc->category = Category::find($ad->category_id)->name;
        $doc->category_id = $ad->category_id;
        $doc->city = City::find($ad->city_id)->name;
        $doc->city_id = $ad->city_id;
        $doc->slug = $ad->slug;
        $doc->status = $ad->status;
        if ($ad->mainPhoto){
            $doc->main_photo = $ad->mainPhoto->file;
        }
        $doc->sold = $ad->sold;
        $doc->views = $ad->views;

        // add the document and a commit command to the update query
        $update->addDocument($doc);
        $update->addCommit();
        $result = $client->update($update);



        return $this->adsList();
    }


    /*+
    * delete ad
    *
    * @return json
    */
    public function deleteAd($ad_id)
    {
        $ad_id = intval($ad_id);
        DB::beginTransaction();
        try{
            $favs =Favorite::where('ad_id',$ad_id)->get();
            foreach($favs as $fav){
                Favorite::destroy($fav->id);
            }
            $photos =Photo::where('ad_id',$ad_id)->get();
            $rotations=['000','090','180','270'];
            $imgdirs=['cards','img','thumb200px','thumb800px'];
            foreach($photos as $photo){
                $filename = $photo->file;
                $fullname = explode(".",$filename);
                $name = $fullname[0];
                $ext = $fullname[1];
                $corename = substr($name,0,strlen($name)-3);
                foreach($rotations as $rotation){
                    foreach($imgdirs as $dir){
                        $photoFile = "storage/img/".$corename . $rotation . $ext;
                        if (file_exists($photoFile)){
                            unlink($photoFile);
                        }
                    }
                }
                Photo::destroy($photo->id);
            }
            Ad::destroy($ad_id);

            //DELETE FROM SOLR
            // create a client instance
            $adapter = new \Solarium\Core\Client\Adapter\Curl();
		    $eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
            $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
            // get an update query instance
            $update = $client->createUpdate();
            // add the delete id and a commit command to the update query
            $update->addDeleteById($ad_id);
            $update->addCommit();
            // this executes the query and returns the result
            $result = $client->update($update);

        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['result'=>'ko']);
        }
        DB::commit();
        return response()->json(['result'=>'ok']);
    }
}
<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;

use App\Ad;
use App\Photo; //ads related photos
use App\City;
use App\Category;

use App\Http\PhotoUtilities;

class PhotoController extends Controller
{
    /**
     * Set photo as main photo (main=1)
     * 
     * Return Boolean 
     */

    public  function setAsmMainPhoto($photo_id){
        return PhotoUtilities::setAsmMainPhoto($photo_id);
    }

    /**
     * Rotate a photo 90º clockwise
     * 
     * Return Json Object (true | false)
     */
    public static function rotatePhoto($photo_id){
        //try{
            $photo= Photo::find($photo_id);
            $degrees = -90;
            if (is_null($photo)){
                return response()->json(['false']);
            }
            $photo_data = explode(".",$photo->file);
            $photo_ext = $photo_data[1];

            $aux= explode(".",$photo->file);
            $fn = explode("_",$aux[0]); //filename structure (array)

            $grads = intval($fn[4]) + ($degrees * -1);
            if ($grads>270){
                $grads = 0;
            }
            
            $old_filename_with_path = public_path("storage/img/").$photo->file;

            $grads = sprintf("%03d",$grads);
            $new_filename = $fn[0] . "_" . $fn[1] . "_" . $fn[2] . "_" . $fn[3] . "_" . $grads . "." . $photo_ext;
            $new_img_with_path = public_path("storage/img/").$new_filename;
            
            if ($photo_ext == 'png'){
                $source = imagecreatefrompng($old_filename_with_path);
                $rotate = imagerotate($source, $degrees, 0);
                imagealphablending($rotate, true);
                imagesavealpha($rotate, TRUE);
                imagepng($rotate, $new_img_with_path);
            }else{
                $source = imagecreatefromjpeg($old_filename_with_path);
                $rotate = imagerotate($source, $degrees, 0);
                imagealphablending($rotate, true);
                imagesavealpha($rotate, TRUE);
                imagejpeg($rotate, $new_img_with_path);
            }        
            PhotoUtilities::savePhoto($new_filename, $photo->order, $photo->ad_id, $photo->main, $photo->id);
       /*  }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([$e->getMessage()]);
        } */
        return response()->json(['true']);
    }
}
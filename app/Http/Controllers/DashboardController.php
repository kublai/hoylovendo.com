<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use ImageResize;

use App\Ad;
use App\Photo; //ads related photos
use App\Favorite;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mark as a sold
     *
     * @return response object
     */
    public function markAsSold($ad_id){
        $user_id_ok = auth()->user()->id; //logged used id
        $ad = Ad::find($ad_id);
        if ($ad->user_id != $user_id_ok){
            return response()->json(['error']); //a user cannot mark as sold an item that is not of his property
        }
        $ad->sold = date('Y-m-d H:i:s');
        if ($ad->save()){
            //update views in SOLR
            $soldAux = new \DateTime($ad->sold);
            $dateTimeSold = $soldAux->format("Y-m-d") . "T" . $soldAux->format("G:i:s") . "Z";
            $adapter = new \Solarium\Core\Client\Adapter\Curl();
		    $eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
            $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
            $update = $client->createUpdate();
            $doc = $update->createDocument();
            $doc->setKey('id',$ad_id);
            $doc->addField('sold', $dateTimeSold);
            $doc->setFieldModifier('sold', 'set');
            $doc->sold = $dateTimeSold;
            $update->addDocument($doc)->addCommit();
            $client->update($update);  
            return response()->json(['ok']); //ad mark as sold
        }else{
            return response()->json(['ko']);
        }
        
    }

    /**
     * Delete ad
     *
     * @return response object
     */
    public function deleteAd($ad_id){
        $favs =Favorite::where('ad_id',$ad_id)->get();
        foreach($favs as $fav){
            Favorite::destroy($fav->id);
        }
        $user_id_ok = auth()->user()->id; //current user id
        $ad = Ad::find($ad_id);
        if ($ad->user_id != $user_id_ok){
            return response()->json(['error']); //a user cannot delete an item that is not from him
        }
        //delete the files for this photo
        $photos = Photo::where('ad_id',$ad_id)->get();
        foreach($photos as $photo){
            $photoFile = $photo->file; 
            if (file_exists("storage/cards/".$photoFile)){
                unlink("storage/cards/".$photoFile);
            }
            if (file_exists("storage/img/".$photoFile)){
                unlink("storage/img/".$photoFile);
            }
            if (file_exists("storage/thumb200px/".$photoFile)){
                unlink("storage/thumb200px/".$photoFile);
            }
            if (file_exists("storage/thumb800px/".$photoFile)){
                unlink("storage/thumb800px/".$photoFile);
            }
            Photo::destroy($photo->id);
        }
        
        //delete ad.
        Ad::destroy($ad_id);

        //DELETE FROM SOLR
        // create a client instance
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
        // get an update query instance
        $update = $client->createUpdate();
        // add the delete id and a commit command to the update query
        $update->addDeleteById($ad_id);
        $update->addCommit();
        // this executes the query and returns the result
        $result = $client->update($update);

        return response()->json(['ok']); //ad mark as sold
    }
}
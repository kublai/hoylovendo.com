<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Ad;
use App\Category;
use App\City;

class FavoriteController extends Controller{

    const ITEMS_PER_PAGE = 16;

    /**
     * Show the favorites page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items_per_page = $this::ITEMS_PER_PAGE;
        if (auth()->user()){
            return view('favorites',compact('items_per_page'));
        }else{
            return redirect('/login');
        }
    }

    /** Get favorites cards data for the logged user
     * 
     * @return json response
     */
    public function getCardsDataFavs(){
        $ads =[];
        if (auth()->user() == null){
            return response()->json($ads);
        }
        $favs = Favorite::where("user_id",auth()->user()->id)->pluck('ad_id')->all();
        $ads = Ad::whereIn('id',$favs)->get();
        foreach($ads as $ad){
            //no way to pass only the name, all the category object will be passed
            $ad['category'] = $ad->category;
            //same with city
            $ad['city'] = $ad->city->name;
            $ad['slug'] = $ad->slug;
            $ad['main_photo'] = $ad->mainPhoto;
            $ad['favorite'] = $ad->favorite;
            $ad['current_user_id']= auth()->user() ? auth()->user()->id : 0 ;
        }
        return response()->json($ads);
    }

    /**
     * creates an entry in the favorites table for the current user if there is not register otherwise delete the register
     * in order to call this function first the session variable "fav_id_tagged"
     * should be initialized
     * 
     * @return array [returnurl, vertical_location]
     */
    public static function swapFavorite()
    {
        if(!\Auth::check()){
            session()->forget(['fav_id_tagged', 'fav_id_currenturl','fav_id_vertlocation']);
            return ['return_url'=>'']; 
        }
        $favorite_flag = intval(session('fav_id_tagged'));
        $return_url = base64_decode(session('fav_id_currenturl'));
        if ($favorite_flag>0){
            $check_fav_exists = Favorite::where(['user_id' => auth()->user()->id, 'ad_id' => $favorite_flag])->count();
            if ($check_fav_exists == 0){ //if the item already exists in favorites for this user
                $fav = new Favorite;
                $fav->user_id = auth()->user()->id;
                $fav->ad_id = $favorite_flag;
                $fav->save();
            }else{
                $fav = Favorite::where(['user_id' => auth()->user()->id, 'ad_id' => $favorite_flag])->first();
                $fav->delete();
            }
            session()->forget(['fav_id_tagged', 'fav_id_currenturl',]);
            return ['return_url'=> $return_url]; ;
        }else{
            return ['return_url'=>'']; 
        }
    }

    /**
     * Mark an ad as favorite for the current user
     * The user is not yet logged. A session variable is created and will be used after the user is autheticated
     * 
     * @param integer ad_id
     * @param string current_url BASE64 encoded
     * @param integer vertical_location
     * 
     * @return Json 
     */
    public function tagFavorite($ad_id, $current_url = null, $vertical_location = null){
        $ad_id = intval($ad_id);
        session(['fav_id_tagged'=> $ad_id]);
        session(['fav_id_currenturl' => $current_url]);
        session(['fav_id_vertlocation' => $vertical_location]);
        if ( $current_url == null ){ //the user is already logged in
            return response()->json(array_merge(['result'=>'ok'],self::swapFavorite()));
        }else{
            return response()->json(['result' =>'ok']);
        }
    }
}
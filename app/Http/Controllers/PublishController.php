<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use ImageResize;

use App\Ad;
use App\Photo; //ads related photos
use App\City;
use App\Category;

use App\Http\PhotoUtilities;

class PublishController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   
     /**
     * User dashboard, list of ads
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $userid = auth()->user()->id;
        $ads = Ad::where('user_id',$userid)->orderby('updated_at','desc')->get();
        return view('dashboard',['ads'=>$ads]);
    }


     /**
     *  Create an add
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function publish()
    {
        $mode="create";
        $ad_id = 0;
        $cities = City::all();
        $categories = Category::all();
        return view('publish',compact('mode','ad_id','cities','categories'));
    }

     /**
     *  Edit an add
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editad($ad_id)
    {
        $mode="edit";
        $ad = Ad::find($ad_id);
        if ($ad->user_id != auth()->user()->id){
            echo "Acceso denegado";die;
        }
        $cities = City::all();
        $categories = Category::all();
        return view('publish',compact('mode','ad','ad_id','cities','categories'));
    }

    /**
     * Receive the data from the step1 of "Publish an ad" 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function publishPost(Request $request)
    {
        request()->validate([
            'título'  => 'required|string|max:100',
            'descripción'  => 'required|string|max:2000',
            'precio'  => 'required|integer|min:1|max:10000000',
            'teléfono'  => 'max:25|regex:/(^[0-9 ]+$)+/',
            'ad_id' => 'required|numeric',
            'mode' => 'required|string|max:10',
            'ciudad_id' => 'required|numeric|min:1',
            'categoria_id' => 'required|numeric|min:1'
        ],
        [
            'título.required' => "El campo 'Título' es requerido",
            'título.string' => "El campo 'Título' debe ser un texto",
            'título.max' => "Máximo, 100 caracteres",
            'descripción.required' => "El campo 'Descripción' es requerido",
            'descripción.string' => "El campo 'Descripción' debe ser un texto",
            'descripción.max' => "Máximo, 2000 caracteres",
            'precio.required' => "El campo 'Precio' es requerido",
            'precio.integer' => "El precio debe ser numérico, sin puntos ni comas",
            'precio.min' => "El precio mínimo es 1",
            'precio.max' => "El precio máximo es 10000000",
            'teléfono.max' => "Máximo 25 caracteres",
            'teléfono.regex' => "Usa solo números, puedes colocar hasta 3 teléfonos separándolos con espacios",
            'ciudad_id.min' => "Elije el Departamento para tu anuncio",
            'categoria_id.min' => "Elije la Categoría para tu anuncio"
        ]);
        $title = $request->input('título');
        $description = $request->input('descripción');
        $price = $request->input('precio');
        $currency = $request->input('moneda');
        $phone = $request->input('teléfono');
        $mode = $request->input('mode');
        $city_id = $request->input('ciudad_id');
        $category_id = $request->input('categoria_id');
       
        if ($mode=="create"){
            $ad = new Ad;
        }else{
            $ad_id = $request->input('ad_id');
            $ad = Ad::find($ad_id);
            if ($ad->user_id != auth()->user()->id){
                echo "Acceso denegado";die;
            }
        }
        $ad->user_id = auth()->user()->id;
        $ad->title = $title;
        $ad->description = $description;
        $ad->price = $price;
        $ad->currency = $currency;
        $ad->phone = $phone;
        $ad->city_id = $city_id;
        $ad->category_id = $category_id;
        $saveOk = $ad->save();
        if (!$saveOk){
            return Redirect::back()->withErrors(['msg', 'No ha sido posible guardar el anuncio, inténtelo nuevamente']);
        }
        // Create or update the register in SOLR
        // create a client instance
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
        $update = $client->createUpdate();
        
        // get an update query instance
        $createdAux = new \DateTime($ad->created_at);
        $dateTimeCreated = $createdAux->format("Y-m-d") . "T" . $createdAux->format("G:i:s") . "Z";
        $boostAux = new \DateTime($ad->boost);
        $dateTimeBoost = $boostAux->format("Y-m-d") . "T" . $boostAux->format("G:i:s") . "Z";
        $updatedAux = new \DateTime($ad->updated_at);
        $dateTimeUpdated = $updatedAux->format("Y-m-d") . "T" . $updatedAux->format("G:i:s") . "Z";
        
        // create a new document for the data
        $doc = $update->createDocument();
        $doc->id = $ad->id;
        $doc->user_id = $ad->user_id;
        $doc->title = $ad->title;
        $doc->description = $ad->description;
        $doc->price = $ad->price;
        $doc->currency = $ad->currency;
        $doc->phone = $ad->phone;
        $doc->category = Category::find($ad->category_id)->name;
        $doc->category_id = $ad->category_id;
        $doc->city = City::find($ad->city_id)->name;
        $doc->city_id = $ad->city_id;
        $doc->slug = $ad->slug;
        $doc->status = $ad->status;
        if ($mode=="create"){
            $doc->main_photo = "---"; //no photos yet at this point.
        }else{
            if ($ad->mainPhoto){
                $doc->main_photo = $ad->mainPhoto->file;
            }  
        }
        $doc->sold = $ad->sold;
        $doc->views = $ad->views;
        $doc->boost = $dateTimeBoost;
        $doc->created_at = $dateTimeCreated;
        $doc->updated_at = $dateTimeUpdated;
        // add the document and a commit command to the update query
        $update->addDocument($doc);
        $update->addCommit();
        $result = $client->update($update);
        //redirect to the correct view
        if ($mode=="create"){
            $ad_id = $ad->id;
            return redirect()->route('publish2',compact('ad_id','mode'));
        }else{
            return redirect()->route('dashboard');
        }
    }


    /**
     * Show the add photos form
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function publish2($ad_id, $mode){
        $user_active = auth()->user()->id;
        $ad = Ad::find($ad_id);
        //control potential hacking point
        if($mode != "edit"){
            if ($mode!="create"){
                echo("Acceso denegado");die;
            }
        }
        if (empty($ad) || $ad->user_id != $user_active){
            echo("Acceso denegado");die;
        }

        return view('publish2',compact('ad_id','mode'));
    }


    /**
     * Receive the photo files and save them 
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function publishPostPhotos(Request $request){
        ini_set('memory_limit', '512M');
        request()->validate([
            'foto'  => 'mimes:jpg,jpeg,png|max:5210',
            'ad_id'  => 'required|numeric',
            'main' => 'required|numeric',
            'counter' => 'required|numeric'
        ]);
        $ad_id = $request->input('ad_id');
        $main_flag = $request->input('main');
        $counter = $request->input('counter');
        if ( $file = $request->file('foto')) {
            $result = PhotoUtilities::savePhoto($file, $counter, $ad_id, $main_flag);
            return $result;
        }else{
            return response()->json(['result'=> false]);
        }
           
        //return Redirect::to("/dashboard")->withSuccess('Ok! tu anuncio esta siendo revisado, será visible en cuanto sea aprobado.');
    }


    /**
     * Edit photos for an ad
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editphotos($ad_id){
        //recover all photos if the ad is in edition mode
        $photos = Photo::where('ad_id',$ad_id)->orderBy('order','ASC')->get();
        $ad = Ad::find($ad_id);
        if ($ad == null || $ad->user_id != auth()->user()->id){
            echo "Accesso denegado";die;
        }
        return view('ads.editphotos',compact('photos','ad'));
    }

    /**
     * delete a photo for an ad
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function deletePhoto($photo_id){
        $user_id = auth()->user()->id;
        $photo = Photo::where(['id'=>$photo_id, 'user_id' => $user_id])->first();
        //check if left just one photo, if thats the case the photo cannot be erased
        $number_photos = Photo::where(['user_id' => $user_id, 'ad_id'=> $photo->ad_id])->count();
        if ($number_photos == 1){
            return response()->json(['ko','Esta es la última imagen, un anuncio debe tener al menos una foto']);
        }
        //check in the photo is the main image, if so abort delete
        if ($photo->main == 1){
            return response()->json(['ko','Esta es la imagen principal, si desea borrarla elija antes otra imagen principal']);
        }
        if ($photo){
            $ad_id_aux = $photo->ad_id;  //save Ad id to reorder photos
            //delete the files for this phote from the system
            $degrees_list = ['000','090','180','270'];
            $auxname = explode("_",$photo->file);
            $auxext = substr($photo->file,-3);
            $photo_file_aux = $auxname[0] . "_" . $auxname[1] ."_" . $auxname[2] . "_" . $auxname[3] . "_";
            foreach($degrees_list as $degrees){
                $photo_file = $photo_file_aux . $degrees . "." . $auxext;
                $auxx = file_exists(public_path("storage/img/") . $photo_file);
                if (file_exists(public_path("storage/img/") . $photo_file)){
                    unlink("storage/img/".$photo_file);
                }
                if (file_exists(public_path("storage/thumb200px/") . $photo_file)){
                    unlink("storage/thumb200px/".$photo_file);
                }
                if (file_exists(public_path("storage/thumb800px/") . $photo_file)){
                    unlink("storage/thumb800px/".$photo_file);
                }
                if (file_exists(public_path("storage/cardds/") . $photo_file)){
                    unlink("storage/cards/".$photo_file);
                }
            }
            Photo::destroy($photo->id);

            //reorder photos
            $this->reorderPhotos($ad_id_aux); //ToDo Log system for errors in reorder
            return response()->json(['ok',"La foto ha sido eliminada"]);
        }else{
            return response()->json(['ko','No ha sido posible eliminar la foto']); //when the photo cannot be deleted we return an empty string for the photo name
        }
    }


    /**
     * Add photos for an ad (form)
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addPhoto($ad_id){
        return view('ads.addphoto',compact('ad_id'));  
    }

    /**
     * Add photos for an ad (form)
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addPhotoPost(Request $request){
        request()->validate([
            'ad_id'  => 'required|numeric',
            'foto1'  => 'mimes:jpg,jpeg,png|max:5210' // change in php.ini to upload_max_filesize = 5M
        ]);
        $ad_id = $request->input('ad_id');
        $counter=1;
        if ($file = $request->file('foto1')){
            PhotoUtilities::savePhoto($file,$counter, $ad_id, 0); //the photo is marked as not main photo by default.
        }
        //reorder photos
        $this->reorderPhotos($ad_id); //ToDo Log system for errors in reorder
        return redirect()->route('editphotos',['ad_id'=>$ad_id]);
    }


    /**
     * Reorder photos for an ad 
     * 
     * @return boolean
     */

    private function reorderPhotos($ad_id){
        try{
            $photosReorder = Photo::where('ad_id',$ad_id)->orderBy('updated_at','DESC')->get();
            $counter=1;
            foreach($photosReorder as $photoaux){
                $photoaux->order = $counter++;
                $photoaux->save(); 
            }
            return true;
        }catch(Exception $e){
            return false;
        }

    }

}
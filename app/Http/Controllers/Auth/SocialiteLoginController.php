<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use App\User;
use App\Favorite;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FavoriteController;

class SocialiteLoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        //$user = Socialite::driver($provider)->user();
        $userSocial = Socialite::driver($provider)->stateless()->user();
        $userAux = User::where(['email' => $userSocial->getEmail()])->first();
        if($userAux){
            \Auth::login($userAux);
        }else{
            $user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'image'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
            ]);
            \Auth::login($user);    
        }
        $favresult = FavoriteController::swapFavorite(); //need a session variable 'fav_id_tagged' to work properly
        if ($favresult['return_url'] != ''){
            return redirect($favresult['return_url'])->with('vertical_location',$favresult['vertical_location']);
        }else{
            //no tagged then proceed to the dashboard
            return redirect()->route('dashboard',['ads'=>[]]);
        }
    }
}
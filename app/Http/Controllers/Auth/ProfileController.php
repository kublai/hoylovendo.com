<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\User;
use App\Ad;

class ProfileController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | User Profile Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling the user profile page.
    |
    */


    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function index(Request $request){
        $title="Mi perfil";
        $user_id = auth()->user()->id;
        $profile = User::find($user_id);
        $ads = Ad::where('user_id',$user_id);
        $number_ads = $ads->count('id');
        $total_views = $ads->sum('views');

        return view('profile',compact('title','profile','number_ads','total_views'));
    }
}

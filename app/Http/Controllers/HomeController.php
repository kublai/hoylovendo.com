<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use App\City;
use App\Photo;
use App\Favorite;

use Illuminate\Http\Request;
use App\Http\PhotoUtilities;

class HomeController extends Controller
{
    const ITEMS_PER_PAGE = 16;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request )
    {
        $items_per_page = $this::ITEMS_PER_PAGE;
        $cat_id = intval($request->cat_id);
        $cat_selected = '';
        $categories = Category::all();
        if ($cat_id>0){
            $filter_category = Category::where('id',$cat_id)->first();
            if ($filter_category !=null){
                $cat_selected = $filter_category->name;
            }else{
                $cat_selected = "";
                $cat_id = 0;
            }
        }
        //$this->getCardsData(16,1,0);
        return view('index', compact('categories','cat_selected', 'cat_id','items_per_page'));
    }

    /**
     * Show the search results.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function searchResults(Request $request)
    {
        $items_per_page = $this::ITEMS_PER_PAGE;
        $categories = Category::all();
        $cities = City::all();
        $searchtext = $request->searchtext;
        $filter_category = $request->categoryfilter;
        $filter_city = $request->cityfilter;
        return view('searchResults',compact('searchtext','categories','cities','filter_category','filter_city','items_per_page'));
    }


    /**
     * Show and indivual ad
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function adDetails($category, $slug)
     {
        $slug_components = explode("-",$slug);
        $ad_id = $slug_components[count($slug_components)-1];
        $ad = Ad::find($ad_id);
        if (is_null($ad)){
            echo("Acceso denegado");die;
        }
        $newviews = $ad->views + 1;
        $title_page = substr($ad->title,0,60);
        $ad->views = $newviews;
        $ad->save();

        //update views in SOLR
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
        $update = $client->createUpdate();
        $doc = $update->createDocument();
        $doc->setKey('id',$ad_id);
        $doc->addField('views', $newviews);
        $doc->setFieldModifier('views', 'set');
        $doc->views = $newviews;
        $update->addDocument($doc)->addCommit();
        $client->update($update);

        $counter = 0; //initialize the counter for photos
        $photos = Photo::where('ad_id', $ad_id)->get();

        //Assure that is only one main image to avoid UI problems
        $photosCtrl = Photo::where(['ad_id'=>$ad_id,'main'=>1])->get();
        if (sizeof($photosCtrl)>1){
            PhotoUtilities::setAsmMainPhoto($photosCtrl[0]->id);
            $photos = Photo::where('ad_id', $ad_id)->get();
        }
        return view('ads.adDetails',compact('ad','photos','counter','title_page'));
     }

    /**
     * return cards data
     *
     * @param integer $items_per_page
     * @param integer $page_number
     * @param integer $cat_id
     * @param string $mode
     * @param string $query
     * 
     * @return Object JSON with all ads data 
     * @return Array  ads
     */
    public function getCardsData($items_per_page, $page_number, $cat_id, $city_id=0,$search_query = "*:*" )
    {
        $items_per_page = intval($items_per_page);
        $page_number = intval($page_number);
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));  
        $query = $client->createSelect();
        $query->setQuery($search_query);
        $query->setStart(0)->setRows($items_per_page*$page_number);
        $query->setFields(array('id','user_id','title','description','price','currency','phone','category','category_id','city','city_id','slug','status','main_photo','sold','views','boost','created_at','updated_at'));
        if ($cat_id>0){
            $filter_category = Category::where('id',$cat_id)->first();
            $query->createFilterQuery('category')->setQuery("category:\"" . $filter_category->name ."\"");
        }
        if ($city_id>0){
            $filter_city = City::where('id',$city_id)->first();
            $query->createFilterQuery('city')->setQuery("city:\"" . $filter_city->name ."\"");
        }
        if ($search_query == "*:*" ){
            $query->addSort('boost', $query::SORT_DESC);
        }
        // executes the query and returns the result
        $resultset = $client->select($query);
        $data =[];
        $counter=0;
        $aux_user_id = auth()->user() ? auth()->user()->id : 0;
        // get all favorites for the loged user
        if ($aux_user_id>0){
            $favorites = Favorite::where('user_id',$aux_user_id)->get();
        }else{
            $favorites = null;
        }
        foreach($resultset as $document){
            $data[$counter] = $document->getFields();
            $data[$counter]['current_user_id'] = $aux_user_id; 
            $data[$counter]['favorite'] = false;
            if ($favorites != null){  
                if ($favorites->contains('ad_id','=',$data[$counter]['id'])){
                    $data[$counter]['favorite'] = true;
                }
            }        
            $counter++;
        }
        return response()->json($data);    
    }

}

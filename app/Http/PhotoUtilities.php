<?php

namespace App\Http;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use ImageResize;

use App\Ad;
use App\Photo; 

Class PhotoUtilities 
{



    /**
     * Save the photo in the correct paths and creates original, 
     * 800px, 200px and Card size versions 
     * 
     * @param object  Request->file object
     * @param integer $counter = 1
     * @param integer $ad_id 
     * @param integer $main 1|0 
     * @param string  $photo_id (used only when updating a photo 0 for new uploads)
     * 
     * @return integer photoId | -1 on error
     */
    public static function savePhoto($file, $counter = 1, $ad_id, $main, $photo_id = 0)
    {
        try{
            $orig_size_path = public_path('storage/img/') ; // path to store uploaded images
            $path_800px = public_path('storage/thumb800px/'); //images with a max with of 800px
            $path_200px = public_path('storage/thumb200px/'); //images with a max with of 200px
            $path_cards = public_path('storage/cards/'); 
            //create the filename or the photo
            
            if(gettype($file) == "string"){ //if we are dealing with a photo that is not uploaded but modified after (rotation)
                $ext_elements = explode(".",$file);
                $fn = explode("_",$ext_elements[0]); //filename structure (array)
                $ext = $ext_elements[1]; 
                $file_name = $file;
                //var_dump($orig_size_path . $file_name);die;
                $img = ImageResize::make($orig_size_path . $file_name);   
            }else{//if the image is uploaded (NEW IMAGE)
                $file_name = auth()->user()->id . "_" . $ad_id . "_" . date('YmdHis') . "_" . $counter . "_000.";
                $ext = $file->getClientOriginalExtension();
                $file_name .= $ext;
                $savedfile = $file->move($orig_size_path, $file_name);
                $img = ImageResize::make($savedfile);
            }   
            
            //create thumbnails
            $img->resize(800,800,function($constraint){
                $constraint->aspectRatio();  
            })->save($path_800px . $file_name); //save thumb og 800px
            $img->resize(200,200,function($constraint){
                $constraint->aspectRatio();  
            })->save($path_200px . $file_name);
            
            //create card
            $img_aux = ($ext =="png") ? imagecreatefrompng($path_800px . $file_name) : imagecreatefromjpeg($path_800px . $file_name);
            $w = imagesx($img_aux);
            $h = imagesy($img_aux);
            if ($w>$h){
                $x = ($w-$h) - ($w-$h)/2; $y = 0;  $w = $h;
            }else{
                $x = 0; $y = ($h-$w) - ($h-$w)/2; $h = $w;
            }
            $img_card = imagecrop($img_aux, ['x' => $x, 'y' => $y, 'width' => $w, 'height' => $h]); 
            if ($ext == 'png') {
                imagepng($img_card, $path_cards . $file_name);
            }else{
                imagejpeg($img_card, $path_cards . $file_name);
            }
            //resize card to 300px if original card ig greater than that.
            if ($w>300){
                $img_card2 = ImageResize::make($path_cards . $file_name);
                $img_card2->resize(300,300)->save($path_cards . $file_name);
            }

            //resize original image if width greater that 1000 px
            $orig_img_path = $orig_size_path . $file_name;
            $img_aux2 = ($ext =="png") ? imagecreatefrompng($orig_img_path) : imagecreatefromjpeg($orig_img_path);
            $w2 = imagesx($img_aux2);
            $h2 = imagesy($img_aux2);
            if ($w2 >1000 || $h2>1000){
                $reduced_orig_img = ImageResize::make($orig_img_path);
                $reduced_orig_img->resize(1000,1000,function($constraint){
                    $constraint->aspectRatio();  
                })->save($orig_img_path); //save original image reduced to 1000px max (HDD saving)
            }

            //update or create register database.
            if(gettype($file) == "string"){ //if the file is not uploaded
                $photo = Photo::find($photo_id);
            }else{
                $photo = new Photo;
            }

            $photo['user_id'] = auth()->user()->id;
            $photo['ad_id']   = $ad_id;
            $photo['file']    = $file_name;
            $photo['order']   = $counter;
            $photo['main']    = $main;
            $photo->save();
            if ($main == 1){ 
                self::setAsmMainPhoto($photo->id);
                self::updateSolrMainPhoto($ad_id, $file_name);
            }
        }catch(\Exception $e){
            \Log::debug( $e->getMessage());
            return response()->json(['result'=> false]);
        }
        return response()->json(['result' => true]);
    }


    /**
     * set the main photo for an add, static method
     * 
     * @return boolean
     */

    public static function setAsmMainPhoto($photo_id){
        $main_photo = Photo::find($photo_id);
        $photos = Photo::where('ad_id',$main_photo->ad_id)->get();
        foreach($photos as $photo){
            if ($photo->id != $photo_id){
                $photo->main=0;
            }else{
                $photo->main=1;
                $aux = self::updateSolrMainPhoto($photo->ad_id, $photo->file);
            } 
            $photo->save();
        }
        return response()->json(['true']);
    }


    /**
     * Update main photo filename in SOLR
     * 
     * @param integer $ad_id
     * @param string $filename
     * 
     * @return boolean
     */
    private static function updateSolrMainPhoto($ad_id,$file_name){
        //UPDATE SOL
        // create a client instance
        $adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
        // get an update query instance
        $update = $client->createUpdate();
        // create a new document for the data
        $doc = $update->createDocument();
        $doc->setKey('id',$ad_id);
        $doc->addField('main_photo', $file_name);
        $doc->setFieldModifier('main_photo', 'set');
        $doc->main_photo = $file_name;
        // add the document and a commit command to the update query
        $update->addDocument($doc)->addCommit();
        return $client->update($update);
    }

}
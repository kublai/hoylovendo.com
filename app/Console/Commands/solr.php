<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Ad;
use App\Category;
use App\City;

class solr extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solr:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a csv file into the solr instance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$ads = Ad::all();
		$adapter = new \Solarium\Core\Client\Adapter\Curl();
		$eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
		
        $client = new \Solarium\Client($adapter, $eventDispatcher, config('solarium'));
        foreach($ads as $ad){
            try{
                $createdAux = new \DateTime($ad->created_at);
                $dateTimeCreated = $createdAux->format("Y-m-d") . "T" . $createdAux->format("G:i:s") . "Z";

                $boostAux = new \DateTime($ad->boost);
                $dateTimeBoost = $boostAux->format("Y-m-d") . "T" . $boostAux->format("G:i:s") . "Z";

                $updatedAux = new \DateTime($ad->updated_at);
                $dateTimeUpdated = $updatedAux->format("Y-m-d") . "T" . $updatedAux->format("G:i:s") . "Z";

                //get an update query instance
                $update = $client->createUpdate();
                // create a new document for the data
                $category = Category::find($ad->category_id);
                $city = City::find($ad->city_id);

                $doc = $update->createDocument();
                $doc->id = $ad->id;
                $doc->user_id = $ad->user_id;
                $doc->title = $ad->title;
                $doc->description = $ad->description;
                $doc->price = $ad->price;
                $doc->currency = $ad->currency;
                $doc->phone = $ad->phone;
                $doc->category = $category->name;
                $doc->category_id = $ad->category_id;
                $doc->city = $city->name;
                $doc->city_id = $ad->city_id;
                $doc->slug = $ad->slug;
                $doc->status = $ad->status;
                if($ad->mainPhoto){
                    $doc->main_photo = $ad->mainPhoto->file;
                }else{
                    $doc->main_photo = "---";
                }
                $doc->sold = $ad->sold;
                $doc->views = $ad->views;
                $doc->boost = $dateTimeBoost;
                $doc->created_at = $dateTimeCreated;
                $doc->updated_at = $dateTimeUpdated;

                // add the document and a commit command to the update query
                $update->addDocument($doc);
                $update->addCommit();
                $result = $client->update($update);
                $this->info($ad->id . ": " . $ad->title );
            }catch(\Exception $e){
                echo ("Error for description: $doc->description ->" . $e->getMessage());
            }
        }
    }
}
